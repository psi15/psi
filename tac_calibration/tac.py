import ROOT
from array import array
import numpy as np
import psiutil
import argparse
import sys

ROOT.gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='')
parser.add_argument('infile', help='input file')
parser.add_argument('outfile', nargs='?', help='output file')
parser.add_argument('-wh', '--width', default=100, type=int)
parser.add_argument('--height', default=5., type=float)

args = parser.parse_args()

file = ROOT.TFile(args.infile)

physics = file.Get('physics')


canvas = ROOT.TCanvas('canvas', 'canvas', 1000, 980)
canvas.Divide(2,2)

canvas.cd(1)

hist = ROOT.TH1D('hist', 'hist', 5100, 900, 6000)
#hist.GetXaxis().SetNdivisions(20)



for event in physics:
    hist.Fill(event.tac1amplitude)

hist.Draw()

bins = []

for i in range(hist.GetNbinsX()+1):
    bins.append((900+i, hist.GetBinContent(i)))


binsSorted = sorted(bins, key=lambda b: b[1])
binsSorted = binsSorted[::-1]


peaks = []

for i in range(0, len(binsSorted)):
    candidate = binsSorted[i]
    if i == 0:
        #print candidate
        #peaks.append(candidate)
        continue

    #print binsSorted[0][1]

    if candidate[1] < args.height:
        continue

    # is it close to another one?
    close = False
    for peak in peaks:
        if abs(candidate[0] - peak[0]) < args.width:
            close = True
            break

    if close:
        continue

    #print candidate[0], peaks[len(peaks)-1][0], abs(candidate[0]-peaks[len(peaks)-1][0])
    #if abs(candidate[0]-peaks[len(peaks)-1][0]) < 10:
        #continue

    peaks.append(candidate)


peaks = sorted(peaks, key=lambda p: p[0])


lines = []

for peak in peaks:
    line = ROOT.TLine(peak[0], 0, peak[0], peak[1])
    line.SetLineColor(ROOT.kRed)
    line.Draw()
    lines.append(line)


print 'number of peaks: ', len(peaks)

canvas.Update()
timesRaw = raw_input('times (evaled): ')
times = eval(timesRaw)

print 'times', times

# TAC1
# times = range(6, 98, 2) + range(100, 130, 2)
# width 30 height 20

#TAC2 times = range(0, 104, 8)
#print times


canvas.cd(2)

corr =  ROOT.TGraphErrors()

for i, t in enumerate(times):
    if i > len(peaks)-1:
        break
    corr.SetPoint(i, t, peaks[i][0])
    corr.SetPointError(i, 0, 5)

# fit the line
line = ROOT.TF1('line', '[0] + [1]*x')
corr.Fit(line, '')

offset, slope = line.GetParameter(0), line.GetParameter(1)


print -offset/slope, 1./slope


corr.Draw('AP')

canvas.cd(4)

ratio = ROOT.TGraph()

for idx, peak in enumerate(peaks):
    #print times[idx], peak[0], offset + slope*times[idx], peak[0]/float(offset + slope*times[idx])
    ratio.SetPoint(idx, times[idx], peak[0]/float(offset + slope*times[idx]))

ratio.Draw('AL')

canvas.Update()

if args.outfile:
    canvas.SaveAs(args.outfile)
else:
    raw_input('>>> Enter for close')
