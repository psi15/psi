//
// Created by Paul Gessinger on 29.12.15.
//

#include "../util.h"

#include "TFile.h"
#include "TTree.h"
#include "TTreeReader.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "TF1.h"
#include "TMultiGraph.h"
#include "TLegend.h"

#include <iostream>
#include <TH1.h>

using namespace std;

double tac1_to_pi(double pi) {
    return 0.01152*pi + 465.48819;
}


int main(int argc, char *argv[]) {

    if(argc < 3) {
        cout << "not enough arguments: infile outfile" << endl;
        return 1;
    }


    char *infileName = argv[1];
    char *outfileName = argv[2];

    cout << "slimming data from " << infileName << " to " << outfileName << endl;

    cout << endl << "==============================" << endl;
    cout << endl;



    TFile *inFile = TFile::Open(infileName);

    auto outFile = new TFile(outfileName, "RECREATE");
    TTree *outTree = new TTree("physics", "physics");
    outTree->SetAutoSave(10e9);

    // clone meta tree and write directly to new file
    auto metaTree = ((TTree*)inFile->Get("meta"))->CloneTree();

    TTreeReader reader("physics", inFile);
    int nEntries = reader.GetEntries(true);

    // READER VALUES
    auto tac1TimeIn = new TTreeReaderValue<double>(reader, "tac1Time");
    auto tac2TimeIn = new TTreeReaderValue<double>(reader, "tac2Time");
    auto calSum = new TTreeReaderValue<double>(reader, "calSumIntegralCalibrated");
    auto pistopTriggerBin = new TTreeReaderValue<double>(reader, "pistopTriggerBin");
    auto pistopNum = new TTreeReaderValue<int>(reader, "pistopNum");
    auto tac1Amplitude = new TTreeReaderValue<double>(reader, "tac1Amplitude");

    NEW_BRANCH(tac1Time, outTree)
    //NEW_BRANCH(tac2Time, outTree)
    NEW_BRANCH(calSumIntegralCalibrated, outTree)

    cout << "looping over " << nEntries << " events" << endl;
    int n=0;

    while(reader.Next()) {

        if(**pistopNum == 1 && **pistopTriggerBin > 200) {

            // check if tac is far away from where it should be (drift?)
            if(fabs(tac1_to_pi(**tac1Amplitude)-**pistopTriggerBin) < 5) {

                tac1Time = **tac1TimeIn;
                //tac2Time = **tac2TimeIn;
                calSumIntegralCalibrated = **calSum;

                outTree->Fill();

            }

        }


        if(n % (nEntries/5) == 0) {
            cout << "  " << std::fixed << std::setw(7) << std::setprecision(2) << 100*n/(double)nEntries << "%" << endl;
        }
        n++;
    }



    metaTree->Write();
    outTree->Write();
    outFile->Close();
    inFile->Close();

    cout << "\r => done      " << endl;



    return 0;

}
