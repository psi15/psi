//
// Created by Paul Gessinger on 29.12.15.
//

#include "../util.h"

#include "TFile.h"
#include "TTree.h"
#include "TTreeReader.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "TF1.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "TStyle.h"

#include <iostream>
#include <TH1.h>

using namespace std;



int main(int argc, char *argv[]) {

    if(argc < 5) {
        cout << "not enough arguments: dataDir, outputDir, runStart, runEnd" << endl;
        return 1;
    }

    bool DEBUG = false;
    if(argc > 5) {
        if(std::string(argv[5]) == "debug") {
            DEBUG = true;
        }
    }

    char *dataDir = argv[1];
    char *outputDir = argv[2];
    int runStart = stoi(argv[3]);
    int runEnd = stoi(argv[4]);

    cout << "looping over files in " << dataDir << " from " << runStart << " to " << runEnd << endl;
    cout << "running in " ;
    if(DEBUG) {
        cout << "DEBUG";
    }
    else {
        cout << "PROD";
    }

    cout << " mode";
    cout << endl << "==============================" << endl;
    cout << endl;

    string filename;
    string outFilename;

    auto c = new TCanvas();

    TMultiGraph *MGchi2Red = new TMultiGraph();
    TGraph *Gchi2RedTac1 = new TGraph();
    TGraph *Gchi2RedTac2 = new TGraph();

    MGchi2Red->SetTitle("#chi^{2}_{red}");

    Gchi2RedTac2->SetLineColor(kBlue);

    MGchi2Red->Add(Gchi2RedTac1);
    MGchi2Red->Add(Gchi2RedTac2);


    TMultiGraph *MGslope = new TMultiGraph();
    TGraph *GslopeTac1 = new TGraph();
    TGraph *GslopeTac2 = new TGraph();

    MGslope->SetTitle("slope");


    GslopeTac2->SetLineColor(kBlue);

    MGslope->Add(GslopeTac1);
    MGslope->Add(GslopeTac2);

    TMultiGraph *MGoffset = new TMultiGraph();
    TGraph *GoffsetTac1 = new TGraph();
    TGraph *GoffsetTac2 = new TGraph();

    MGoffset->SetTitle("offset");

    GoffsetTac2->SetLineColor(kBlue);

    MGoffset->Add(GoffsetTac1);
    MGoffset->Add(GoffsetTac2);

    int graphN = 0;

    unsigned int invalidRunN = 0;

    int droppedRuns = 0;


    for(int i = runStart; i <= runEnd; i++) {

        // open files in current directory
        filename = Form("%spsi15_run_%06d.root", dataDir, i);

        double perc =  100*(i-runStart)/(double)(runEnd-runStart);
        if(DEBUG) {
            cout << "--------------" << endl;
            cout << std::fixed << std::setw(7) << std::setprecision(2) << perc;
            cout << "% : RUN " << i << " => " << filename << endl;
            cout << "invalid: " << invalidRunN << std::fixed << std::setw(7) << std::setprecision(2) << 100*(invalidRunN)/(double)(i-runStart) << "%" << endl;
            cout << "--------------" << endl;
        }
        else {

            if(i%20 == 0) {
                cout << std::fixed << std::setw(7) << std::setprecision(2) << perc << "%" << " (RUN: " << i << ", invalid: " ;
                cout << invalidRunN << ", " << std::fixed << std::setw(7) << std::setprecision(2) << 100*(invalidRunN)/(double)(i-runStart) << "%)" << endl;
            }
        }

        //check if file exists
        if(!exists_test(filename)) {
            if(DEBUG) {
                dout("file " << filename << " does not exist");
                //cout << "file " << filename << " does not exist" << endl;
            }

            invalidRunN++;

            continue;
        }


        outFilename = Form("%spsi15_run_%06d.root", outputDir, i);

        dout("reading from " << filename)
        dout("writing to " << outFilename)

        TFile *file = TFile::Open(filename.c_str());
        TTree *inTree = (TTree*) file->Get("physics");
        inTree->SetAutoSave(10e9);



        TH1D pistopTriggerBinHist("pistopTriggerBinHist", "pistopTriggerBinHist", 120, 450, 570);
        TH1D pistop2TriggerBinHist("pistop2TriggerBinHist", "pistop2TriggerBinHist", 120, 450, 570);

        TH2D tac1VSpistopHist("tac1VSpistopHist", "tac1VSpistopHist", 2000, 0, 8000, 1024, 0, 1024);
        TH2D tac1VSpistop2Hist("tac1VSpistop2Hist", "tac1VSpistop2Hist", 2000, 0, 8000, 1024, 0, 1024);
        TH2D tac2VSpistopHist("tac2VSpistopHist", "tac2VSpistopHist", 2000, 0, 8000, 1024, 0, 1024);
        TH2D tac2VSpistop2Hist("tac2VSpistop2Hist", "tac2VSpistop2Hist", 2000, 0, 8000, 1024, 0, 1024);

        TGraph tac1VSPistopGraph;
        TGraph tac2VSPistopGraph;

        TTreeReader reader("physics", file);
        int nEntries = reader.GetEntries(true);

        // READER VALUES
        auto tac1Amplitude = new TTreeReaderValue<double>(reader, "tac1Amplitude");
        auto tac2Amplitude = new TTreeReaderValue<double>(reader, "tac2Amplitude");
        auto pistopTriggerBin = new TTreeReaderValue<double>(reader, "pistopTriggerBin");
        auto pistop2TriggerBin = new TTreeReaderValue<double>(reader, "pistop2TriggerBin");

        //Long64_t nEntries = inTree->GetEntries();
        dout("looping over " << nEntries << " events")
        int n=0;

        TF1* f1 = new TF1("f1", "[0]*x + [1]", 0, 8000);
        f1->SetParName(0, "slope");
        f1->SetParName(1, "offset");

        TF1* f2 = new TF1("f2", "[0]*x + [1]", 0, 4000);
        f2->SetParName(0, "slope");
        f2->SetParName(1, "offset");

        vector<pair<double, int>> tac1PistopVec;
        vector<pair<double, int>> tac2PistopVec;

        while(reader.Next()) {


            pistopTriggerBinHist.Fill(**pistopTriggerBin);
            pistop2TriggerBinHist.Fill(**pistop2TriggerBin);

            if(**pistopTriggerBin > 470) {


                tac1VSpistopHist.Fill(**tac1Amplitude, **pistopTriggerBin);

                if(n%10 == 0) {
                    //tac1VSPistopGraph.SetPoint(graphN1, **tac1Amplitude, **pistopTriggerBin);
                    //graphN1++;
                    tac1PistopVec.push_back(make_pair(**tac1Amplitude, **pistopTriggerBin));
                }


            }

            if(**pistopTriggerBin > 530 && 500 < **tac2Amplitude && **tac2Amplitude < 3500) {
                tac2VSpistopHist.Fill(**tac2Amplitude, **pistopTriggerBin);

                if(n%10 == 0 ) {
                    tac2PistopVec.push_back(make_pair(**tac2Amplitude, **pistopTriggerBin));
                    //tac2VSPistopGraph.SetPoint(graphN2, **tac2Amplitude, **pistopTriggerBin);
                    //graphN2++;
                }
            }

            if(n % (nEntries/5) == 0) {
                if(DEBUG) {
                    cout << "  " << std::fixed << std::setw(7) << std::setprecision(2) << 100*n/(double)nEntries << "%" << endl;
                }
                //cout << "\r *  " << std::fixed << std::setw(7) << std::setprecision(2) << 100*n/(double)nEntries << "%" << flush;
            }
            n++;
        }


        // fill graphs with sorted vectors
        sort(tac1PistopVec.begin(), tac1PistopVec.end());
        sort(tac2PistopVec.begin(), tac2PistopVec.end());

        for(unsigned int j=0;j<tac1PistopVec.size();j++) {
            tac1VSPistopGraph.SetPoint(j, tac1PistopVec.at(j).first, tac1PistopVec.at(j).second);
        }

        for(unsigned int j=0;j<tac2PistopVec.size();j++) {
            tac2VSPistopGraph.SetPoint(j, tac2PistopVec.at(j).first, tac2PistopVec.at(j).second);
        }




        tac1VSPistopGraph.Fit("f1", "");
        tac2VSPistopGraph.Fit("f2", "");

        double chi2redTac1 = f1->GetChisquare()/(double)f1->GetNDF();
        double chi2redTac2 = f2->GetChisquare()/(double)f2->GetNDF();

        double slopeTac1 = f1->GetParameter("slope");
        double slopeTac2 = f2->GetParameter("slope");
        double offsetTac1 = f1->GetParameter("offset");
        double offsetTac2 = f2->GetParameter("offset");

        // decide if we keep this run

        if(chi2redTac1 > 2. || chi2redTac2 > 2) {
            dout("skipping run due to high chi2red")
            continue;
        }



        // hard coded from test run
        if(fabs(offsetTac1-465.48819) > 0.5 || fabs(offsetTac2-542.47135) > 0.5) {
            droppedRuns++;
            continue;
        }

        if(fabs(slopeTac1-0.01152) > 0.005 || fabs(slopeTac2-0.00304) > 0.005) {
            droppedRuns++;
            continue;
        }

        //TFile outFile(outFilename.c_str(), "RECREATE");
        auto outFile = new TFile(outFilename.c_str(), "RECREATE");

        dout("cloning existing tree (this may take a while)")
        TTree *outTree = inTree->CloneTree();
        outTree->SetAutoSave(10e9);

        // clone meta tree and write directly to new file
        auto metaTree = ((TTree*)file->Get("meta"))->CloneTree();

        double tac1Amplitude_alt;
        inTree->SetBranchAddress("tac1Amplitude", &tac1Amplitude_alt);

        double tac2Amplitude_alt;
        inTree->SetBranchAddress("tac2Amplitude", &tac2Amplitude_alt);



        NEW_BRANCH(tac1Time, outTree)
        NEW_BRANCH(tac2Time, outTree)



        Gchi2RedTac1->SetPoint(graphN, i, chi2redTac1);
        Gchi2RedTac2->SetPoint(graphN, i, chi2redTac2);
        GoffsetTac1->SetPoint(graphN, i, offsetTac1);
        GoffsetTac2->SetPoint(graphN, i, offsetTac2);
        GslopeTac1->SetPoint(graphN, i, slopeTac1);
        GslopeTac2->SetPoint(graphN, i, slopeTac2);

        graphN++;

        /*print(chi2redTac1)
        print(chi2redTac2)
        print(Form("%06f*x + %06f", f1->GetParameter("slope"), f1->GetParameter("offset")))
        print(Form("%06f*x + %06f", f2->GetParameter("slope"), f2->GetParameter("offset")))*/

        // iterate again over events and apply calibration from fit

        for(int n = 0; n<nEntries;n++) {
            inTree->GetEntry(n);

            //print(tac1Amplitude_alt)
            //print(tac2Amplitude_alt)
            //cout << std::fixed << std::setw(7) << std::setprecision(10) << tac1Amplitude_alt << endl;

            tac1Time = tac1Amplitude_alt*slopeTac1 + offsetTac1;
            tac2Time = tac2Amplitude_alt*slopeTac2 + offsetTac2;

            //print(tac1Time)
            //print(tac2Time)

            btac1Time->Fill();
            btac2Time->Fill();

            //cout << "second run tree reader " << n << endl;
        }


        metaTree->Write();
        outTree->Write();
        outFile->Close();
        file->Close();

        if(DEBUG) {
            cout << "\r => done      " << endl;
        }


        delete file;
        delete outFile;

        if(DEBUG) {

            c->Clear();
            gStyle->SetOptFit();

            tac1VSPistopGraph.GetYaxis()->SetTitle("tac1 amplitude");
            tac1VSPistopGraph.GetXaxis()->SetTitle("#pi-stop trigger bin");
            tac1VSPistopGraph.GetYaxis()->SetRangeUser(450, 600);

            tac1VSPistopGraph.Draw("AL");

            c->SaveAs(Form("debug/run_%06d_tac1VSPistopGraph.pdf", i));

            /*c->Clear();

            tac2VSPistopGraph.Draw();
            c->SaveAs(Form("debug/run_%06d_tac2VSPistopGraph.pdf", i));*/

            c->Clear();

            pistopTriggerBinHist.Draw();
            c->SaveAs(Form("debug/run_%06d_pistopTriggerBinHist.pdf", i));

            pistop2TriggerBinHist.Draw();
            c->SaveAs(Form("debug/run_%06d_pistop2TriggerBinHist.pdf", i));

            c->Clear();
            c->Divide(1, 2);

            c->cd(1);
            tac1VSPistopGraph.Draw("AP");

            c->cd(2);
            tac2VSPistopGraph.Draw("AP");

            c->SaveAs(Form("debug/run_%06d_tacsVSpistopGraph.pdf", i));

            c->Clear();

            c->Divide(1, 2);
            c->cd(1);
            tac1VSpistopHist.Draw("colz");

            c->cd(2);
            tac2VSpistopHist.Draw("colz");

            c->SaveAs(Form("debug/run_%06d_tacsVSpistopHist.pdf", i));


        }
    }

    print(droppedRuns)


    // plot tac graphs

    __("mean chi2redtac1 is " << std::fixed << std::setw(10) << std::setprecision(5) << Gchi2RedTac1->GetMean(2))
    __("mean chi2redtac2 is " << std::fixed << std::setw(10) << std::setprecision(5) << Gchi2RedTac1->GetMean(2))
    __("mean slope tac1 is  " << std::fixed << std::setw(10) << std::setprecision(5) << GslopeTac1->GetMean(2))
    __("mean slope tac2 is  " << std::fixed << std::setw(10) << std::setprecision(5) << GslopeTac2->GetMean(2))
    __("mean offset tac1 is " << std::fixed << std::setw(10) << std::setprecision(5) << GoffsetTac1->GetMean(2))
    __("mean offset tac2 is " << std::fixed << std::setw(10) << std::setprecision(5) << GoffsetTac2->GetMean(2))




    c->Clear();
    c->SetWindowSize(800, 5*600);
    c->Divide(1, 3);

    TLegend *legend = new TLegend(0.15, 0.85, 0.25, 0.95);
    legend->AddEntry(Gchi2RedTac1, "tac1", "L");
    legend->AddEntry(Gchi2RedTac2, "tac2", "L");
    legend->Draw();

    c->cd(1);

    MGchi2Red->Draw("A");

    MGchi2Red->GetYaxis()->SetTitle("#chi^{2}_{red}");
    MGchi2Red->GetXaxis()->SetTitle("run");

    c->cd(2);

    MGslope->Draw("A");

    MGslope->GetYaxis()->SetTitle("slope");
    MGslope->GetXaxis()->SetTitle("run");

    c->cd(3);

    MGoffset->Draw("A");

    MGoffset->GetYaxis()->SetTitle("offset");
    MGoffset->GetXaxis()->SetTitle("run");

    c->SaveAs("runGraphs.pdf");



    /*string inFilename = argv[1];
    string outFilename = argv[2];

    cout << "reading from " << inFilename << endl;
    cout << "writing to " << outFilename << endl;

    TFile *file = TFile::Open(inFilename.c_str());

    TTree *inTree = (TTree*) file->Get("physics");
    inTree->SetAutoSave(10e9);

    TFile outFile(outFilename.c_str(), "RECREATE");

    cout << "cloning existing tree (this may take a while)" << endl;
    TTree *outTree = inTree->CloneTree();
    outTree->SetAutoSave(10e9);

    //TTree *outTree = new TTree("physics", "physics");

    // clone meta tree and write directly to new file
    auto metaTree = ((TTree*)file->Get("meta"))->CloneTree();



    // set up new and old branches we need for the calibration
    // this uses two macros defined above

    EX_BRANCH(tac1Amplitude, inTree)
    EX_BRANCH(tac2Amplitude, inTree)
    EX_BRANCH(pistopTriggerBin, inTree)
    EX_BRANCH(pistop2TriggerBin, inTree)


    NEW_BRANCH(tac1Time, outTree)
    NEW_BRANCH(tac2Time, outTree)

    auto c = new TCanvas();

    TH1D pistopTriggerBinHist("pistopTriggerBinHist", "pistopTriggerBinHist", 100, 450, 550);
    TH1D pistop2TriggerBinHist("pistop2TriggerBinHist", "pistop2TriggerBinHist", 100, 450, 550);
*/
    /*Long64_t nEntries = inTree->GetEntries();
    cout << "looping over " << nEntries << " events" << endl;
    for(int i=0;i<nEntries;i++) {

        inTree->GetEntry(i);

        pistopTriggerBinHist.Fill(pistopTriggerBin);
        pistop2TriggerBinHist.Fill(pistop2TriggerBin);



        if(i % (nEntries/40) == 0) {
            cout << std::fixed << std::setw(7) << std::setprecision(2) << 100*i/(double)nEntries << "%" << endl;
        }
    }

    // write tree to disk
    metaTree->Write();
    outTree->Write();
    outFile.Close();*/


    cout << "done" << endl;

    return 0;

}
