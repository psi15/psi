import ROOT
from array import array

def channelArrayToHistogram(ch, name, title=None):
    title = title if title != None else name
    hist = ROOT.TH1D(name, title, len(ch), 1, len(ch)+1)

    for idx, value in enumerate(ch):
        hist.SetBinContent(idx, value)

    return hist

def histogramToChannelArray(hist):
    ch = []
    for b in range(hist.GetNbinsX()):
        ch.append(int(hist.GetBinContent(b)))
    return ch

def pprinttable(rows):
  if len(rows) > 1:
    headers = rows[0]._fields
    lens = []
    for i in range(len(rows[0])):
      lens.append(len(max([x[i] for x in rows] + [headers[i]],key=lambda x:len(str(x)))))
    formats = []
    hformats = []
    for i in range(len(rows[0])):
      if isinstance(rows[0][i], int):
        formats.append("%%%dd" % lens[i])
      else:
        formats.append("%%-%ds" % lens[i])
      hformats.append("%%-%ds" % lens[i])
    pattern = " | ".join(formats)
    hpattern = " | ".join(hformats)
    separator = "-+-".join(['-' * n for n in lens])
    print hpattern % tuple(headers)
    print separator
    _u = lambda t: t.decode('UTF-8', 'replace') if isinstance(t, str) else t
    for line in rows:
        print pattern % tuple(_u(t) for t in line)
  elif len(rows) == 1:
    row = rows[0]
    hwidth = len(max(row._fields,key=lambda x: len(x)))
    for i in range(len(row)):
      print "%*s = %s" % (hwidth,row._fields[i],row[i])


def convert_timestamp(tsWin):
    tsDiff = 578568*60*60
    return tsWin-tsDiff


def draw(ch, opt = ''):
    print 'drawing histogramm'
    return channelArrayToHistogram(ch, 0)

def smoothChannels(ch, smoothWidth):
    nCh = len(ch)
    chSmoothed = array('i', [0] * nCh)
    #histSmoothed = ROOT.TH1D(str(os.urandom(32)), '', 2048, 1, 2049)


    for i in range(smoothWidth, len(ch)-smoothWidth):
        value = 0

        for s in range(-smoothWidth, smoothWidth):
            value += ch[i+s]
        value /= 2*smoothWidth+1

        chSmoothed[i] = int(value)

    return chSmoothed

def dosChannels(ch, padding, stepSize):
    nCh = len(ch)
    chDos = array('i', [0]*nCh)
    #print nCh

    for i in range(padding+1+stepSize, len(ch)-padding-stepSize, stepSize):
        chDos[i] = ch[i]-ch[i-stepSize]

    return chDos

def kill(obj):
    obj.IsA().Destructor(obj)
