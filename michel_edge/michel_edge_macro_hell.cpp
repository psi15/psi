/*
* First analysis of PSI15 data
Fit of Michel spectra with theoretical function convoluted with Gaussian



*/
#include "TSystem.h"
#include "TH1F.h"
#include "TFile.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "TString.h"
#include <iostream>
#include <vector>
#include "TCanvas.h"
#include "TMath.h"
#include "TF1.h"
#include "TStyle.h"
#include <sys/stat.h>
#include "TGraph.h"
#include "TMultiGraph.h"


#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooFFTConvPdf.h"
#include "RooPlot.h"
using namespace RooFit;


using namespace ROOT;
using namespace std;




//check if file exists
bool exists_test (const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

//Gaussian multplied with Michel function
//first parameter: convolution variable, second: maximum energy, third: normalization, fourth: sigma
Double_t michelgauss(Double_t *x, Double_t *par)

{
  Float_t xx =x[0];
  if(xx < par[1]){
    Double_t f = (3.*pow(xx/par[1], 2) - 2.*pow(xx/par[1], 3))*par[2] * exp(-0.5*pow((par[0]-xx)/par[3],2));
    //Double_t f = exp(-1.*pow((par[0]-xx)/par[3],2));
    return f;
  }
  else return 0.;


}

Double_t analytical_convolution(Double_t *xvar, Double_t *par) {
  double a = par[0];
  double mu = par[1];
  double sigma = par[2];
  double f = par[3];
  double A = par[4];
  double B = par[5];

  double min = par[6];
  double y = xvar[0];

  if(y < min) {
    return 0;
  }

  //double res = a*((3*sqrt(2*M_PI)*(pow(x - mu), 2) + pow(sigma, 2))/sqrt(1/pow(sigma, 2)) - (2*sqrt(2*M_PI)*(x - mu))*(pow(x - mu), 2) + 3*pow(sigma, 2)))/sqrt(1/pow(sigma, 2)));
  //double res = (-a)*sqrt(2*M_PI)*sigma*((-3 + 2*x - 2*mu)*pow(x - mu, 2) + 3*(-1 + 2*x - 2*mu)*pow(sigma, 2));
  double res = (a*sqrt(2*M_PI)*sigma*(-2*B*pow(y - mu, 3) + 6*B*(-y + mu)*pow(sigma, 2) + 3*A*f*(pow(y - mu, 2) + pow(sigma, 2))))/pow(f, 3);

  res = res < 0 ? 0 : res;

  return res;
}



// convolution of Gaussian with Michel function
//first: maximum energy, second: normalization, third: sigma

Double_t convolution(Double_t *t, Double_t *par){
  Double_t tt=t[0];
  TF1 * test = new TF1("test", michelgauss, 0, 50000, 4);
  test->SetParameter(0,tt);
  test->SetParameter(1,par[0]);
  test->SetParameter(2,par[1]);
  test->SetParameter(3,par[2]);

  Double_t f = test->Integral(tt - 10 * par[2], tt + 10 * par[2], 200);

  return f;

}

// ./main first_run_number last_run_number
// files have to be in same directory
int main(int argc, char *argv[]) {
  gStyle->SetOptFit(1);


  TCanvas *c = new TCanvas();
  TF1 *conv = new TF1("conv", analytical_convolution, 0, 20000, 7);
  conv->SetParameter(0, 0.00544346);
  conv->SetParameter(1, -3298.64);
  conv->SetParameter(2, 4954.26);
  conv->SetParameter(3, 17658.);
  conv->SetParameter(4, 4.1906);
  conv->SetParameter(5, 4.82782);
  conv->SetParameter(6, 0);


  conv->GetYaxis()->SetRangeUser(0, 300);

  conv->Draw();

  //c->SaveAs("test.pdf");


  //return 0;

  if(argc < 5) {
    cout << "not enough arguments: dataDir, outputDir, runStart, runEnd" << endl;
    return 1;
  }

  char *dataDir = argv[1];
  char *outputDir = argv[2];
  int runStart = stoi(argv[3]);
  int runEnd = stoi(argv[4]);

  cout << "looping over files in " << dataDir << " from " << runStart << " to " << runEnd << endl;

  string filename;
  vector<double> runnumbers;
  vector<vector<double>> integrals;
  vector<vector<double>> michel_edges;

  //loop over all file numbers
  for(int i = runStart; i <= runEnd; i++){
    // open files in current directory
    filename = Form("%spsi15_run_%06d.root", dataDir, i);

    cout << filename << endl;
    //check if file exists
    if(!exists_test(filename)) {
      cout << "file " << filename << " does not exist" << endl;
      //return 1;
    }
    else {
      runnumbers.push_back(i);
      TFile * file = TFile::Open(filename.c_str());


      TH1F *Htac1amplitude = new TH1F("Htac1amplitude", "Htac1amplitudee", 100, 6040, 6060);

      //set up vector of histograms for calorimeter integrals
      vector<TH1F*> histovector;
      for(int j = 0; j < 7; j++){
        histovector.push_back(new TH1F(Form("cal%dIntegral",j), Form("cal%dIntegral",j), sqrt(24000), 1000, 25000));
      }
      //cout<<histovector.size()<<endl;

      //set vector of  TTreeReader values for integrals for each calorimeter
      TTreeReader reader("physics", file);
      vector<TTreeReaderValue<double>*> integralvector;
      for(int j = 0; j < 7; j++){
        integralvector.push_back(new TTreeReaderValue<double>(reader, Form("cal%dIntegral",j+1)));
      }

      //set up convolution for fit
      TF1 *poly = new TF1("poly","[0] + [1] * x + [2]*x**2 +[3]*x**3  ",50000,75000);
      // TF1* michel = new TF1("michel","(3.*(x/[1])**2 - 2.*(x/[1])**3)*[0] ",12000,18000);
      // TF1* folded_michel = new TF1
      // TF1 * test = new TF1("test", "michelgauss( x, 1, 2, ,3 ,4, 0, 50)");




      TTreeReaderValue<double> tac1amplitude(reader, "tac1Amplitude");



      while (reader.Next()) {
        Htac1amplitude->Fill(*tac1amplitude);
        for(int j = 0; j < histovector.size(); j++){
          histovector.at(j)->Fill(**integralvector.at(j));
        }
      }


      TCanvas * c1 = new TCanvas("michel", "michel", 800, 800);
      c1->Divide(2,4);
      TPad *p1 = (TPad *)(c1->cd(1));
      vector<double> endpoints;
      vector<double> maxima;
      //p1->SetLogy();

      c1->SetLogy(1);

      integrals.resize(7);
      michel_edges.resize(7);

      TH1F* hist;
      double max;
      double edge_approx;
      TF1 *michel = new TF1("michel", analytical_convolution, 5000, 25000, 7);
      //TF1 *michel_deriv =

      for(int j = 0; j < histovector.size(); j++){

        c1->cd(j+1);
        c1->SetLogy(1);
        hist = histovector.at(j);
        hist->Draw();

        max = hist->GetMaximum();
        edge_approx = hist->GetXaxis()->GetBinCenter(hist->FindLastBinAbove(max/2.));

        /*for(int i=1;i<=hist->GetNbinsX();++i) {
        cout << (i>1?",":"") << "{" << i << "," << hist->GetBinContent(i) << "}";
      }

      cout << endl;*/


      /*michel->SetParameter(0, 1);
      michel->SetParameter(1, 2.5);
      michel->SetParameter(2, 0.4);
      michel->SetParameter(3, 13000);
      michel->FixParameter(4, 10000);*/

      michel->SetParameter(0, 0.00544346);
      michel->SetParameter(1, -3298.64);
      michel->SetParameter(2, 4954.26);
      michel->SetParameter(3, 17658.);
      michel->SetParameter(4, 4.1906);
      michel->SetParameter(5, 4.82782);
      michel->FixParameter(6, 5000);

      michel->SetParName(0, "a");
      michel->SetParName(1, "mu");
      michel->SetParName(2, "sigma");
      michel->SetParName(3, "f");
      michel->SetParName(4, "A");
      michel->SetParName(5, "B");
      michel->SetParName(6, "min");



      hist->Fit(michel, "RM");

      //get Michel edges from fit
      integrals.at(j).push_back(michel->GetParameter(1));
      michel_edges.at(j).push_back(michel->GetParameter(3));
      TPad *p1 = (TPad *)(c1->cd(j+1));
      endpoints.push_back(michel->GetX(0.,14000,25000));
      maxima.push_back(hist->GetXaxis()->GetBinLowEdge(hist->GetMaximumBin()));












      // free the fit function
      //delete michel;
    }

    //reset reader
    TTreeReader reader2("physics", file);
    vector<TTreeReaderValue<double>*> integralvector2;
    for(int k = 0; k < 7; k++){
      integralvector2.push_back(new TTreeReaderValue<double>(reader2, Form("cal%dIntegral",k+1)));
    }
    //calibration value
    double calibrated_edge = 105.6583715/2.; // half of m_mu from http://pdg.lbl.gov/2015/listings/rpp2015-list-muon.pdf


    //fill tree with calibrated spectra



    TFile hfile(Form("%srun_%06d_calo_calibrated.root", outputDir, i), "RECREATE");


    TTree *outTree = new TTree("physics","");
    //TTree *outTree = (TTree*) file->Get("physics")->Clone();


    #define BRANCH(name, type, suffix, roottype)   type name suffix; \
                                TBranch * b ## name = outTree->Branch(#name,&name, TString(#name + roottype)));

    #define CALBRANCH(name, type) BRANCH(cal ## 1 ## name, type, , "/D") \
                                  BRANCH(cal ## 2 ## name, type, , "/D") \
                                  BRANCH(cal ## 3 ## name, type, , "/D") \
                                  BRANCH(cal ## 4 ## name, type, , "/D") \
                                  BRANCH(cal ## 5 ## name, type, , "/D") \
                                  BRANCH(cal ## 6 ## name, type, , "/D") \
                                  BRANCH(cal ## 7 ## name, type, , "/D") \


    CALBRANCH(IntegralCalibrated, double)
    CALBRANCH(Integral, double)
    CALBRANCH(Uncertainty, double)
    CALBRANCH(BaselineDeviation, double)

    BRANCH(calSumIntegral, double, , "/D")
    BRANCH(calSumUncetrainty, double, , "/D")
    BRANCH(calSumBaselineDeviation, double, , "/D")

    BRANCH(tac1Amplitude, double, , "/D")
    BRANCH(tac1AmplitudeUncertainty, double, , "/D")
    BRANCH(tac2Amplitude, double, , "/D")
    BRANCH(tac2AmplitudeUncertainty, double, , "/D")

    BRANCH(pistopNum, int, , "/D")
    BRANCH(pistopTriggerBin, double, [10], "/D")
    BRANCH(pistop2Num, int, , "/D")
    BRANCH(pistop2TriggerBin, double, [10], "/D")

    BRANCH(Sc6Num, int, , "/D")
    BRANCH(Sc6TriggerBin, double, [10], "/D")
    BRANCH(Sc4Num, int, , "/D")
    BRANCH(Sc4TriggerBin, double, [10], "/D")

    BRANCH(eventIDBox1, int, , "/D")
    BRANCH(eventCntBox1, int, , "/D")
    BRANCH(eventTsRawBox1, int, , "/D")
    BRANCH(eventDataLengthBox1, int, , "/D")

    BRANCH(eventIDBox2, int, , "/D")
    BRANCH(eventCntBox2, int, , "/D")
    BRANCH(eventTsRawBox2, int, , "/D")
    BRANCH(eventDataLengthBox2, int, , "/D")

    BRANCH(internalEventID, int, , "/D")


    while (reader2.Next()) {


      cal1IntegralCalibrated = calibrated_edge / michel_edges.at(0).back() * **integralvector2.at(0);
      cal2IntegralCalibrated = calibrated_edge / michel_edges.at(1).back() * **integralvector2.at(1);
      cal3IntegralCalibrated = calibrated_edge / michel_edges.at(2).back() * **integralvector2.at(2);
      cal4IntegralCalibrated = calibrated_edge / michel_edges.at(3).back() * **integralvector2.at(3);
      cal5IntegralCalibrated = calibrated_edge / michel_edges.at(4).back() * **integralvector2.at(4);
      cal6IntegralCalibrated = calibrated_edge / michel_edges.at(5).back() * **integralvector2.at(5);
      cal7IntegralCalibrated = calibrated_edge / michel_edges.at(6).back() * **integralvector2.at(6);


      outTree->Fill();

    }


    outTree->Write();
    hfile.Close();







    c1->SaveAs("./michel_fits.pdf");


    for(int j = 0; j < endpoints.size(); j++){




    }










  }
  cout<<"--------------"<<i<<endl;
}



vector<TGraph*> graphs;
TMultiGraph * multigraph = new TMultiGraph("mg", "mg");

for(int k = 0; k < michel_edges.size(); k++){
  graphs.push_back(new TGraph(runnumbers.size(), runnumbers.data(), michel_edges.at(k).data()));
  graphs.at(k)->SetTitle(Form("Calo %d",k));
  graphs.at(k)->SetLineColor(k+1);
  multigraph->Add(graphs.at(k));

}
cout<<runnumbers.size()<<endl;
cout<<michel_edges.size()<<endl;
cout<<michel_edges.at(0).size()<<endl;
cout<<michel_edges.at(5).size()<<endl;
TGraph * graph = new TGraph(runnumbers.size(), runnumbers.data(), michel_edges.at(0).data());
TCanvas * canvas = new TCanvas();

multigraph->Draw("ALP");
multigraph->SetTitle("Michel edge position over run number");
multigraph->GetXaxis()->SetTitle("run number");
multigraph->GetYaxis()->SetTitle("Michel edge position");
canvas->BuildLegend();
canvas->SaveAs("./michel_fits_over_runs.pdf");


return 0;
}
