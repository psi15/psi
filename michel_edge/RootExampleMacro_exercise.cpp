/*
 * Example macro for simple ROOT tasks
 * Authors: Simon Corrodi, Dorothea vom Bruch
 * August 2014
 * Modified August 2015

 */

#include "TH1F.h"
#include "TFile.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include <iostream>
#include <vector>
#include "TCanvas.h"

using namespace ROOT::Math;
using namespace std;

void RootExampleMacro_exercise( ) {
  //open file
  TFile * file = TFile::Open("../convert/psi15_run_0000.root");



  TH1F *Htac1amplitude = new TH1F("Htac1amplitude", "Htac1amplitudee", 100, 6040, 6060);

  //set up vector of integral histograms
  vector<TH1F*> histovector;
  for(int i = 0; i < 7; i++){
    histovector.push_back(new TH1F(Form("cal%dintegral",i), Form("cal%dintegral",i), 100, 262000, 264000));
  }

  //set vector of integrals
  TTreeReader reader("physics", file);
  vector<TTreeReaderValue<double>*> integralvector;
  for(int i = 0; i < 7; i++){
    integralvector.push_back(new TTreeReaderValue<double>(reader, Form("cal%dintegral",i+1)));
  }


  TTreeReaderValue<double> * Integrals [8];


  TTreeReaderValue<double> tac1amplitude(reader, "tac1amplitude");
  TTreeReaderValue<double> cal1integral(reader, "cal1integral");
  Integrals[1] = new TTreeReaderValue<double>(reader,"cal2integral");

  while (reader.Next()) {
    Htac1amplitude->Fill(*tac1amplitude);
    for(int j = 0; j < histovector.size(); j++){
      histovector.at(j)->Fill(**integralvector.at(j));
    }
    
  }

  TCanvas * c1 = new TCanvas();
  c1->Divide(2,4);


  for(int j = 0; j < histovector.size(); j++){
    c1->cd(j+1);
    histovector.at(j)->Draw();
    }
 

}





