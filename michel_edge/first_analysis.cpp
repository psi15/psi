/*
 * First analysis of PSI15 data


 */
#include "TSystem.h"
#include "TH1F.h"
#include "TFile.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include <iostream>
#include <vector>
#include "TCanvas.h"
#include "TMath.h"
#include "TF1.h"
#include "TStyle.h"
#include <sys/stat.h>
#include "TGraph.h"
#include "TMultiGraph.h"


using namespace ROOT;
using namespace std;

bool exists_test (const std::string& name) {
    struct stat buffer;
    return (stat (name.c_str(), &buffer) == 0);
}
//first parameter: convolusion variable, second: maximum energy, third: normalization, fourth: sigma
Double_t michelgauss(Double_t *x, Double_t *par)
   {
      Float_t xx =x[0];
      Double_t f = (3.*pow(xx/par[1], 2) - 2.*pow(xx/par[1], 3))*par[2] * exp(-1.*pow((par[0]-xx)/2./par[3],2));
      //Double_t f = exp(-1.*pow((par[0]-xx)/par[3],2));
      return f;
   }
//first: maximum energy, second: normalization, third: sigma

Double_t convolution(Double_t *t, Double_t *par){
    Double_t tt=t[0];
    TF1 * test = new TF1("test", michelgauss, 0, 50000, 4);
    test->SetParameter(0,tt);
    test->SetParameter(1,par[0]);
    test->SetParameter(2,par[1]);
    test->SetParameter(3,par[2]);

    Double_t f = test->Integral(tt - 5 * par[2], tt + 5 * par[2],10);

    return f;

}


int main(int argc, char *argv[]) {
    gStyle->SetOptFit(1);
    //gSystem->Load("libTreeViewer.so");
    //open file
    Double_t as[1] = {1.};
    Double_t pars[4]={1,2,3};
    cout<<convolution(as,pars)<<endl;
    cout<<stoi(argv[1])+50<<endl;
    cout<<exists_test(Form("./psi15_run_%06d.root",3515))<<endl;
    string filename;
    vector<double> runnumbers;
    vector<vector<double>> integrals;
    for(int i = stoi(argv[1]); i <= stoi(argv[2]); i++){

        filename = Form("/home/floh/PSI2015/psi/convert/to_convert/psi15_run_%06d.root",i);
        if(exists_test(filename)){
            runnumbers.push_back(i);
            TFile * file = TFile::Open(filename.c_str());



            TH1F *Htac1amplitude = new TH1F("Htac1amplitude", "Htac1amplitudee", 100, 6040, 6060);

            //set up vector of integral histograms
            vector<TH1F*> histovector;
            for(int j = 0; j < 7; j++){
                histovector.push_back(new TH1F(Form("cal%dIntegral",j), Form("cal%dIntegral",j), 200, 1000, 25000));
            }
            //cout<<histovector.size()<<endl;

            //set vector of integrals
            TTreeReader reader("physics", file);
            vector<TTreeReaderValue<double>*> integralvector;
            for(int j = 0; j < 7; j++){
                integralvector.push_back(new TTreeReaderValue<double>(reader, Form("cal%dIntegral",j+1)));
            }
            //cout<<integralvector.size()<<endl;

            TF1 *poly = new TF1("poly","[0] + [1] * x + [2]*x**2 +[3]*x**3  ",50000,75000);
           // TF1* michel = new TF1("michel","(3.*(x/[1])**2 - 2.*(x/[1])**3)*[0] ",12000,18000);
            TF1 * michel = new TF1("michel",convolution,8000,20000,3);
           // TF1* folded_michel = new TF1
             // TF1 * test = new TF1("test", "michelgauss( x, 1, 2, ,3 ,4, 0, 50)");
              //test->Integral(0,50);

           // TF1 *fa1 = new TF1("fa1","sin(y)/y",0,10);



            TTreeReaderValue<double> tac1amplitude(reader, "tac1Amplitude");
            TTreeReaderValue<double> cal1integral(reader, "cal1Integral");


            while (reader.Next()) {
                Htac1amplitude->Fill(*tac1amplitude);
                for(int j = 0; j < histovector.size(); j++){
                    histovector.at(j)->Fill(**integralvector.at(j));
                }

            }

            TCanvas * c1 = new TCanvas();
            c1->Divide(2,4);
            TPad *p1 = (TPad *)(c1->cd(1));
            vector<double> endpoints;
            vector<double> maxima;
            //p1->SetLogy();

            c1->SetLogy(1);

            integrals.resize(7);
            for(int j = 0; j < histovector.size(); j++){
                c1->cd(j+1);
                c1->SetLogy(1);
                histovector.at(j)->Draw();


                michel->SetParameter(0, 17000);
                michel->SetParameter(1, 0.5);
                michel->SetParameter(2,250);



                histovector.at(j)->Fit(michel, "R");

                integrals.at(j).push_back(michel->GetParameter(1));
                TPad *p1 = (TPad *)(c1->cd(j+1));
                endpoints.push_back(michel->GetX(0.,14000,25000));
                maxima.push_back(histovector.at(j)->GetXaxis()->GetBinLowEdge(histovector.at(j)->GetMaximumBin()));

                //p1->SetLogy();
            }
            c1->SaveAs("./test.pdf");

           // cout<<"Calo   Endpoint  Maximum"<<endl;
            for(int j = 0; j < endpoints.size(); j++){
               // cout<<"Calo"<<j<<": "<<endpoints.at(j)<<"   "<<maxima.at(j)<<endl;


/*
                TCanvas * c1 = new TCanvas();
                c1->Divide(2,4);
                TPad *p1 = (TPad *)(c1->cd(1));
                vector<double> endpoints;
                vector<double> maxima;
                //p1->SetLogy();

                c1->SetLogy(1);

                for(int j = 0; j < integrals.size(); j++){

                    poly->SetParameter(3,-10);
                    michel->SetParameter(0, 13495.6);
                    michel->SetParameter(1, 14638.4);

                    histovector.at(j)->Fit(michel, "RQ");

                 //   endpoints.push_back(michel->GetX(0.,14000,25000));
                   // maxima.push_back(histovector.at(j)->GetXaxis()->GetBinLowEdge(histovector.at(j)->GetMaximumBin()));

                    //p1->SetLogy();
                }
               // cout<<"Calo   Endpoint  Maximum"<<endl;
                for(int j = 0; j < endpoints.size(); j++){
                //    cout<<"Calo"<<j<<": "<<endpoints.at(j)<<"   "<<maxima.at(j)<<endl;
                }
                c1->SaveAs("./test.pdf");

*/
            }










        }
    cout<<"--------------"<<i<<endl;
    }



  vector<TGraph*> graphs;
  TMultiGraph * multigraph = new TMultiGraph("mg", "mg");

  for(int k = 0; k < integrals.size(); k++){
    graphs.push_back(new TGraph(runnumbers.size(), runnumbers.data(), integrals.at(k).data()));
    graphs.at(k)->SetTitle(Form("Calo %d",k));
    graphs.at(k)->SetLineColor(k+1);
    multigraph->Add(graphs.at(k));

  }
  cout<<runnumbers.size()<<endl;
  cout<<integrals.size()<<endl;
  cout<<integrals.at(0).size()<<endl;
  cout<<integrals.at(5).size()<<endl;
  TGraph * graph = new TGraph(runnumbers.size(), runnumbers.data(), integrals.at(0).data());
  TCanvas * canvas = new TCanvas();
  multigraph->Draw("ALP");
  canvas->BuildLegend();
  canvas->SaveAs("./test2.pdf");
  cout<<"convo: "<<convolution(as,pars)<<endl;

    return 0;
}





