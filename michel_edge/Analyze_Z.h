//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Apr  4 15:55:28 2014 by ROOT version 5.34/05
// from TTree toyMC/Mass construction data tree
// found on file: drellyan.root
//////////////////////////////////////////////////////////

#ifndef Analyze_Z_h
#define Analyze_Z_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.

class Analyze_Z {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   vector<float>   *cal_E;
   vector<float>   *cal_emfrac;
   vector<float>   *cal_phi;
   vector<float>   *cal_eta;
   vector<float>   *trk_px;
   vector<float>   *trk_py;
   vector<float>   *trk_pz;

   // List of branches
   TBranch        *b_cal_E;   //!
   TBranch        *b_cal_emfrac;   //!
   TBranch        *b_cal_phi;   //!
   TBranch        *b_cal_eta;   //!
   TBranch        *b_trk_px;   //!
   TBranch        *b_trk_py;   //!
   TBranch        *b_trk_pz;   //!

   Analyze_Z(TTree *tree=0);
   virtual ~Analyze_Z();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Analyze_Z_cxx
Analyze_Z::Analyze_Z(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("ntuple_1e6_slim.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("ntuple_1e6_slim.root");
      }
      f->GetObject("toyMC",tree);

   }
   Init(tree);
}

Analyze_Z::~Analyze_Z()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Analyze_Z::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Analyze_Z::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Analyze_Z::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   cal_E = 0;
   cal_emfrac = 0;
   cal_phi = 0;
   cal_eta = 0;
   trk_px = 0;
   trk_py = 0;
   trk_pz = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("cal_E", &cal_E, &b_cal_E);
   fChain->SetBranchAddress("cal_emfrac", &cal_emfrac, &b_cal_emfrac);
   fChain->SetBranchAddress("cal_phi", &cal_phi, &b_cal_phi);
   fChain->SetBranchAddress("cal_eta", &cal_eta, &b_cal_eta);
   fChain->SetBranchAddress("trk_px", &trk_px, &b_trk_px);
   fChain->SetBranchAddress("trk_py", &trk_py, &b_trk_py);
   fChain->SetBranchAddress("trk_pz", &trk_pz, &b_trk_pz);
   Notify();
}

Bool_t Analyze_Z::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Analyze_Z::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Analyze_Z::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Analyze_Z_cxx
