#! /usr/bin/env python
import struct
import datetime as dt
import ctypes
import ROOT
import argparse
import numpy as n
import sys
import re
import multiprocessing as mp
import time
import ConfigParser
import inspect
import os
from array import array

import integrate
import amplitude_plus_minus
import rising_flank_position
import psiutil



from array import array

def branch(dtype=float):
  return n.zeros(1, dtype=dtype)

def parseNumList(string):
    m = re.match(r'(\d+)(?:-(\d+))?$', string)
    # ^ (or use .split('-'). anyway you like.)
    if not m:
        raise argparse.ArgumentTypeError("'" + string + "' is not a range of number. Expected forms like '0-5' or '2'.")
    start = m.group(1)
    end = m.group(2) or start
    return list(range(int(start,10), int(end,10)+1))

def process_channel(info):
    if info['output']:
        print '-'*20
        print 'process ', info['name']
        print '-'*20

    if info['type'] == 'tac':
        info['result'] = amplitude_plus_minus.amplitude_plus_minus(info['data'], output=info['output'])
    elif info['type'] == 'calo':
        info['result'] = integrate.integrate(info['data'], output=info['output'])
    elif info['type'] == 'scint':
        info['result'] = rising_flank_position.rising_flank_position(info['data'], output=info['output'])
    else:
      raise ValueError('Invalid channel type')


    return info



def main():
    start = time.time()

    parser = argparse.ArgumentParser(description='Convert binary file from LabView to ROOT')
    parser.add_argument('file',  nargs='?', help='input file')
    parser.add_argument('-p', '--processes', help='how many processes do you want?', default=mp.cpu_count()/2, type=int)
    parser.add_argument('-d', '--diagnostics', help="do diagnostics plots for specified events in root out file", type=parseNumList)
    parser.add_argument('-r', '--range', help="limit to specified events", type=parseNumList)
    parser.add_argument('--dump-default-cfg', dest='dump_config', action='store_true')
    parser.add_argument('-i', '--interactive', action='store_true')
    parser.add_argument('-o', '--output', action='store_true')

    args = parser.parse_args()

    if args.dump_config:
      config = ConfigParser.ConfigParser()

      config.add_section('main')
      config.set('main', 'processes', 0)

      integrate_options = {
        'smoothWidth': 3,
        'stepSize': 16,
        'flankFraction': 5/100.,
        'acceptableBaselineDeviation': 10/100.,
        'maxBaselineDeviation': 20/100.
      }

      flank_options = {
        'smoothWidth': 3,
        'stepSize': 16,
        'flankFraction': 8/100.,
        'triggerFraction': 1.
      }

    #   for key in channels:
    #     config.add_section(key)
    #     for valKey in channels[key]:
    #       config.set(key, valKey, channels[key][valKey])

      with open('config.cfg', 'wb') as configfile:
        config.write(configfile)

      return


    if not args.file:
      raise ValueError('no input file given')

    print "convert binary file from LabView to ROOT"

    inFile = open(args.file, "rb")

    inFileSize = os.path.getsize(args.file)

    bytesRead = 0

    print 'will read %d bytes'%inFileSize

    header = struct.unpack('<IccccccccIIIIII', inFile.read(4*9))
    bytesRead += 9*4

    if args.interactive:
        canv = ROOT.TCanvas('interactive', 'interactive', 1600, 1600)
        canv.Divide(3, 5)

    size, nRun, tsWin, pBeam, confNum, actCh, numSam = (header[0:1] + header[9:15])

    tsUnix = psiutil.convert_timestamp(tsWin)

    datetime = dt.datetime.fromtimestamp(tsUnix)

    ts = datetime.strftime("%d.%m.%Y %H:%M:%S")

    print ''
    print '='*56
    print 'size', 'nRun', 'date'+15*' ', 'pBeam', 'confNum', 'actCh', 'numSam'
    print '-'*56
    print '% 4d % 4d %s % 5d % 7d % 5d % 6d'%(size, nRun, ts, pBeam, confNum, actCh, numSam)
    print '='*56
    print ''
    print ''

    print 'Event header read, reading events:'


    events = []
    eventMeta = []

    histCache = []
    wfCanvases = []

    outFileName = 'psi15_run_%06d.root'%nRun

    if not args.interactive:
        outFile = ROOT.TFile(outFileName, 'RECREATE')


    # write run meta data
    meta = ROOT.TTree('meta', 'meta')

    nRunM = n.zeros(1, dtype=int)
    meta.Branch('nRun', nRunM, 'nRun/I')
    nRunM[0] = 80

    pBeamM = n.zeros(1, dtype=float)
    meta.Branch('pBeam', pBeamM, 'pBeam/D')
    pBeamM[0] = pBeam

    confNumM = n.zeros(1, dtype=int)
    meta.Branch('confNum', confNumM, 'confNum/I')
    confNumM[0] = confNum

    actChM = n.zeros(1, dtype=int)
    meta.Branch('actCh', actChM, 'actCh/I')
    actChM[0] = actCh

    numSamM = n.zeros(1, dtype=int)
    meta.Branch('numSam', numSamM, 'numSam/I')
    actChM[0] = actCh



    meta.Fill()
    meta.Write()


    tree = ROOT.TTree('physics', 'physics')

    # TAC1
    tac1Amplitude = branch()
    tac1AmplitudeUncertainty = branch()

    tree.Branch('tac1Amplitude', tac1Amplitude, 'tac1Amplitude/D')
    tree.Branch('tac1AmplitudeUncertainty', tac1AmplitudeUncertainty, 'tac1AmplitudeUncertainty/D')


    # TAC2
    tac2Amplitude = branch()
    tac2AmplitudeUncertainty = branch()

    tree.Branch('tac2Amplitude', tac2Amplitude, 'tac2Amplitude/D')
    tree.Branch('tac2AmplitudeUncertainty', tac2AmplitudeUncertainty, 'tac2AmplitudeUncertainty/D')


    # CAL1
    cal1Integral = branch()
    cal1Uncertainty = branch()
    cal1BaselineDeviation = branch()


    tree.Branch('cal1Integral', cal1Integral, 'cal1Integral/D')
    tree.Branch('cal1Uncertainty', cal1Uncertainty, 'cal1Uncertainty/D')
    tree.Branch('cal1BaselineDeviation', cal1BaselineDeviation, 'cal1BaselineDeviation/D')



    # CAL2
    cal2Integral = branch()
    cal2Uncertainty = branch()
    cal2BaselineDeviation = branch()


    tree.Branch('cal2Integral', cal2Integral, 'cal2Integral/D')
    tree.Branch('cal2Uncertainty', cal2Uncertainty, 'cal2Uncertainty/D')
    tree.Branch('cal2BaselineDeviation', cal2BaselineDeviation, 'cal2BaselineDeviation/D')


    # CAL3
    cal3Integral = branch()
    cal3Uncertainty = branch()
    cal3BaselineDeviation = branch()

    tree.Branch('cal3Integral', cal3Integral, 'cal3Integral/D')
    tree.Branch('cal3Uncertainty', cal3Uncertainty, 'cal3Uncertainty/D')
    tree.Branch('cal3BaselineDeviation', cal3BaselineDeviation, 'cal3BaselineDeviation/D')


    # CAL4
    cal4Integral = branch()
    cal4Uncertainty = branch()
    cal4BaselineDeviation = branch()

    tree.Branch('cal4Integral', cal4Integral, 'cal4Integral/D')
    tree.Branch('cal4Uncertainty', cal4Uncertainty, 'cal4Uncertainty/D')
    tree.Branch('cal4BaselineDeviation', cal4BaselineDeviation, 'cal4BaselineDeviation/D')


    # CAL5
    cal5Integral = branch()
    cal5Uncertainty = branch()
    cal5BaselineDeviation = branch()

    tree.Branch('cal5Integral', cal5Integral, 'cal5Integral/D')
    tree.Branch('cal5Uncertainty', cal5Uncertainty, 'cal5Uncertainty/D')
    tree.Branch('cal5BaselineDeviation', cal5BaselineDeviation, 'cal5BaselineDeviation/D')


    # CAL6
    cal6Integral = branch()
    cal6Uncertainty = branch()
    cal6BaselineDeviation = branch()

    tree.Branch('cal6Integral', cal6Integral, 'cal6Integral/D')
    tree.Branch('cal6Uncertainty', cal6Uncertainty, 'cal6Uncertainty/D')
    tree.Branch('cal6BaselineDeviation', cal6BaselineDeviation, 'cal6BaselineDeviation/D')


    # CAL7
    cal7Integral = branch()
    cal7Uncertainty = branch()
    cal7BaselineDeviation = branch()

    tree.Branch('cal7Integral', cal7Integral, 'cal7Integral/D')
    tree.Branch('cal7Uncertainty', cal7Uncertainty, 'cal7Uncertainty/D')
    tree.Branch('cal7BaselineDeviation', cal7BaselineDeviation, 'cal7BaselineDeviation/D')


    # CALsum
    calSumIntegral = branch()
    calSumUncertainty = branch()
    calSumBaselineDeviation = branch()
    tree.Branch('calSumIntegral', calSumIntegral, 'calSumIntegral/D')
    tree.Branch('calSumUncertainty', calSumUncertainty, 'calSumUncertainty/D')
    tree.Branch('calSumBaselineDeviation', calSumBaselineDeviation, 'calSumBaselineDeviation/D')


    # Pistop copy
    pistop2TriggerBin = n.zeros(10, dtype=float)
    pistop2Num = n.zeros(1, dtype=int)

    tree.Branch('pistop2Num', pistop2Num, 'pistop2Num/I')
    tree.Branch('pistop2TriggerBin', pistop2TriggerBin, 'pistop2TriggerBin[pistop2Num]/D')


    # Sc6
    Sc6TriggerBin = n.zeros(10, dtype=float)
    Sc6Num = n.zeros(1, dtype=int)

    tree.Branch('Sc6Num', Sc6Num, 'Sc6Num/I')
    tree.Branch('Sc6TriggerBin', Sc6TriggerBin, 'Sc6TriggerBin[Sc6Num]/D')


    # Sc4
    Sc4TriggerBin = n.zeros(10, dtype=float)
    Sc4Num = n.zeros(1, dtype=int)

    tree.Branch('Sc4Num', Sc4Num, 'Sc4Num/I')
    tree.Branch('Sc4TriggerBin', Sc4TriggerBin, 'Sc4TriggerBin[Sc4Num]/D')

    # pistop
    pistopTriggerBin = n.zeros(10, dtype=float)
    pistopNum = n.zeros(1, dtype=int)

    tree.Branch('pistopNum', pistopNum, 'pistopNum/I')
    tree.Branch('pistopTriggerBin', pistopTriggerBin, 'pistopTriggerBin[pistopNum]/D')


    eventIDBox1 = n.zeros(1, dtype=int)
    eventCntBox1 = n.zeros(1, dtype=int)
    eventTsRawBox1 = n.zeros(1, dtype=int)
    eventDataLengthBox1 = n.zeros(1, dtype=int)
    eventIDBox2 = n.zeros(1, dtype=int)
    eventCntBox2 = n.zeros(1, dtype=int)
    eventTsRawBox2 = n.zeros(1, dtype=int)
    eventDataLengthBox2 = n.zeros(1, dtype=int)


    tree.Branch('eventIDBox1', eventIDBox1, 'eventIDBox1/I')
    tree.Branch('eventCntBox1', eventCntBox1, 'eventCntBox1/I')
    tree.Branch('eventTsRawBox1', eventTsRawBox1, 'eventTsRawBox1/I')
    tree.Branch('eventDataLengthBox1', eventDataLengthBox1, 'eventDataLengthBox1/I')
    tree.Branch('eventIDBox2', eventIDBox2, 'eventIDBox2/I')
    tree.Branch('eventCntBox2', eventCntBox2, 'eventCntBox2/I')
    tree.Branch('eventTsRawBox2', eventTsRawBox2, 'eventTsRawBox2/I')
    tree.Branch('eventDataLengthBox2', eventDataLengthBox2, 'eventDataLengthBox2/I')

    internalEventID = n.zeros(1, dtype=int)
    tree.Branch('internalEventID', internalEventID, 'internalEventID/I')


    nEvent = 0

    firstEventTSB1=0
    firstEventTSB2=0

    procCnt = args.processes if not args.interactive else 1

    pool = mp.Pool(procCnt)
    print '=> starting up %d worker processes'%procCnt


    working = ['-', '\\', '|', '/']
    workingI = 0

    while True:
        # IS THIS AN EVENT OR ARE WE DONE?
        inFile.seek(4, 1) # size of array, should be 5

        #Q, = struct.unpack('<Q', inFile.read(8))
        Q = n.fromfile(inFile, dtype=n.uint64, count=1)

        bytesRead += 12

        if str(hex(Q)[2:-1]) != 'b100b100affeaffe':
            #print 'break b100'
            inFile.seek(-12, 1)
            bytesRead -= 12
            break

        if nEvent%1000 == 0:
            elapsed = time.time()-start
            mbRead = bytesRead/1024./1024.
            out = "\r"+working[workingI%4]+' %9.2f mb (%6.2f mb/s) read (% 6.2f%%) at event % 10d, time elapsed % 10.2fs, per event % 8.2fms'%(mbRead, mbRead/elapsed, (100*bytesRead/float(inFileSize)), nEvent+1, elapsed, 1000.*elapsed/(nEvent+1))
            if args.interactive:
                out += "\n"
            sys.stdout.write(out)
            sys.stdout.flush()

            workingI += 1
        nEvent += 1

        #eventIDB1, eventTsB1, eventCntB1, eventDataLengthB1 = struct.unpack('<IIII', inFile.read(4*4))
        eventIDB1, eventTsB1, eventCntB1, eventDataLengthB1 = n.fromfile(inFile, dtype=n.uint32, count=4)

        #if args.interactive:
        #    print eventIDB1, eventTsB1, eventCntB1, eventDataLengthB1

        #dataB1 = struct.unpack("<%uH"%eventDataLengthB1, inFile.read(2*eventDataLengthB1))
        dataB1 = n.fromfile(inFile, dtype=n.uint16, count=eventDataLengthB1)



        inFile.seek(4, 1) # size of array, should be 5

        Q = n.fromfile(inFile, dtype=n.uint64, count=1)


        if str(hex(Q)[2:-1]) != 'b200b2001b001b00':
            raise 'break corrupt'

        #eventIDB2, eventTsB2, eventCntB2, eventDataLengthB2 = struct.unpack('<IIII', inFile.read(4*4))
        eventIDB2, eventTsB2, eventCntB2, eventDataLengthB2 = n.fromfile(inFile, dtype=n.uint32, count=4)

        #if args.interactive:
        #    print eventIDB2, eventTsB2, eventCntB2, eventDataLengthB2

        #dataB2 = struct.unpack("<%uH"%eventDataLengthB2, inFile.read(2*eventDataLengthB2))
        dataB2 = n.fromfile(inFile, dtype=n.uint16, count=eventDataLengthB2)

        inFile.seek(4, 1) # size of array, should be 2

        Q = n.fromfile(inFile, dtype=n.uint64, count=1)

        if str(hex(Q)[2:-1]) != 'effaeffa2b002b00':
            raise 'break corrupt'

        bytesRead += 16+2*eventDataLengthB1+64+2*eventDataLengthB2+12

        # timestamp / reference timestamp
        if nEvent == 1:
            firstEventTsB1 = eventTsB1
            firstEventTsB2 = eventTsB2

        if args.range and not nEvent in args.range:
            continue


        channelsB1 = dataB1.reshape((2048,7)).T
        channelsB2 = dataB2.reshape((2048,7)).T

        channels = n.concatenate((channelsB1, channelsB2))

        channels = channels[:,0:numSam]


        ### WRITE TO ROOT FILE

        chProc = [
            {'type': 'tac', 'name': 'tac1', 'data': channels[0], 'output': args.output},
            {'type': 'tac', 'name': 'tac2', 'data': channels[7], 'output': args.output},
            {'type': 'calo', 'name': 'calo1', 'data': channels[1], 'output': args.output},
            {'type': 'calo', 'name': 'calo2', 'data': channels[2], 'output': args.output},
            {'type': 'calo', 'name': 'calo3', 'data': channels[3], 'output': args.output},
            {'type': 'calo', 'name': 'calo4', 'data': channels[4], 'output': args.output},
            {'type': 'calo', 'name': 'calo5', 'data': channels[8], 'output': args.output},
            {'type': 'calo', 'name': 'calo6', 'data': channels[9], 'output': args.output},
            {'type': 'calo', 'name': 'calo7', 'data': channels[10], 'output': args.output},
            {'type': 'calo', 'name': 'sum_calo', 'data': channels[12], 'output': args.output},
            {'type': 'scint', 'name': 'scint3', 'data': channels[5], 'output': args.output},
            {'type': 'scint', 'name': 'scint6', 'data': channels[6], 'output': args.output},
            {'type': 'scint', 'name': 'scint4', 'data': channels[11], 'output': args.output},
            {'type': 'scint', 'name': 'pistop', 'data': channels[13], 'output': args.output},
        ]

        if procCnt > 1:
            chProc = pool.map(process_channel, chProc)
        else:
            chProc = map(process_channel, chProc)

        if args.interactive:
            print chProc[0]['result']

        # write event physics data to tree

        tac1Amplitude[0] =            chProc[0]['result'][0]
        tac1AmplitudeUncertainty[0] = chProc[0]['result'][1]

        tac2Amplitude[0] =            chProc[1]['result'][0]
        tac2AmplitudeUncertainty[0] = chProc[1]['result'][1]


        cal1Integral[0] =             chProc[2]['result'][0]
        cal1Uncertainty[0] =          chProc[2]['result'][1]
        cal1BaselineDeviation[0] =    chProc[2]['result'][2]

        cal2Integral[0] =             chProc[3]['result'][0]
        cal2Uncertainty[0] =          chProc[3]['result'][1]
        cal2BaselineDeviation[0] =    chProc[3]['result'][2]

        cal3Integral[0] =             chProc[4]['result'][0]
        cal3Uncertainty[0] =          chProc[4]['result'][1]
        cal3BaselineDeviation[0] =    chProc[4]['result'][2]

        cal4Integral[0] =             chProc[5]['result'][0]
        cal4Uncertainty[0] =          chProc[5]['result'][1]
        cal4BaselineDeviation[0] =    chProc[5]['result'][2]

        cal5Integral[0] =             chProc[6]['result'][0]
        cal5Uncertainty[0] =          chProc[6]['result'][1]
        cal5BaselineDeviation[0] =    chProc[6]['result'][2]

        cal6Integral[0] =             chProc[7]['result'][0]
        cal6Uncertainty[0] =          chProc[7]['result'][1]
        cal6BaselineDeviation[0] =    chProc[7]['result'][2]

        cal7Integral[0] =             chProc[8]['result'][0]
        cal7Uncertainty[0] =          chProc[8]['result'][1]
        cal7BaselineDeviation[0] =    chProc[8]['result'][2]

        calSumIntegral[0] =           chProc[9]['result'][0]
        calSumUncertainty[0] =        chProc[9]['result'][1]
        calSumBaselineDeviation[0] =  chProc[9]['result'][2]


        pistop2Num[0] =               len(chProc[10]['result'])
        for i in range(0, 10):
            if i < len(chProc[10]['result']):
                pistop2TriggerBin[i] =    chProc[10]['result'][i]
            else:
                pistop2TriggerBin[i] =    -1

        Sc6Num[0] =                   len(chProc[11]['result'])
        for i in range(0, 10):
            if i < len(chProc[11]['result']):
                Sc6TriggerBin[i] =    chProc[11]['result'][i]
            else:
                Sc6TriggerBin[i] =    -1

        Sc4Num[0] =                   len(chProc[12]['result'])
        for i in range(0, 10):
            if i < len(chProc[12]['result']):
                Sc4TriggerBin[i] =    chProc[12]['result'][i]
            else:
                Sc4TriggerBin[i] =    -1

        pistopNum[0] =                len(chProc[13]['result'])
        for i in range(0, 10):
            if i < len(chProc[13]['result']):
                pistopTriggerBin[i] =    chProc[13]['result'][i]
            else:
                pistopTriggerBin[i] =    -1


        # write event meta data to tree
        eventIDBox1[0] = eventIDB1
        eventCntBox1[0] = eventCntB1
        eventTsRawBox1[0] = eventTsB1
        eventDataLengthBox1[0] = eventDataLengthB1
        eventIDBox2[0] = eventIDB2
        eventCntBox2[0] = eventCntB2
        eventTsRawBox2[0] = eventTsB2
        eventDataLengthBox2[0] = eventDataLengthB2

        internalEventID[0] = nEvent

        tree.Fill()

        # is this one of the first 10 events? write to file for diag
        if (args.diagnostics and nEvent in args.diagnostics) or args.interactive:
            #for chN, ch in enumerate(channels):
                #h = psiutil.channelArrayToHistogram(ch, 'event%d_ch%d'%(nEvent, chN))
                #waveforms.append(h)


            if not args.interactive:
                canv = ROOT.TCanvas('event_%d'%nEvent, 'event_%d'%nEvent, 2)
                canv.Divide(3, 5)
            if args.interactive:
                canv.Clear('D')

            for chN, ch in enumerate(channels):
                #print chN
                canv.cd(chN+1)
                h = psiutil.channelArrayToHistogram(ch, 'event_%d_ch_%d'%(nEvent, chN), 'channel %d'%chN)
                histCache.append(h)
                h.Draw()


            canv.cd(15)
            # draw some info on there
            x = 0.0
            textSize = 0.1
            tRunNumber = ROOT.TText(x, 0.8, 'Run Number: %04d'%nRun)
            tRunNumber.SetTextSize(textSize)
            tRunNumber.Draw()
            tEventNumber = ROOT.TText(x, 0.7, 'Event Number: %d'%nEvent)
            tEventNumber.SetTextSize(textSize)
            tEventNumber.Draw()
            tpBeam = ROOT.TText(x, 0.6, 'Beam Momentum: %d'%pBeam)
            tpBeam.SetTextSize(textSize)
            tpBeam.Draw()
            tConfNum = ROOT.TText(x, 0.5, 'Conf Number: %d'%confNum)
            tConfNum.SetTextSize(textSize)
            tConfNum.Draw()
            tRunDT = ROOT.TText(x, 0.4, 'Run DT: %s'%ts)
            tRunDT.SetTextSize(textSize)
            tRunDT.Draw()

            tEventTSRawB1 = ROOT.TText(x, 0.3, 'Event TS Raw Box 1: %d'%eventTsB1)
            tEventTSRawB1.SetTextSize(textSize)
            tEventTSRawB1.Draw()

            eventTSRelB1 = eventTsB1 - firstEventTsB1
            eventTSRelB2 = eventTsB2 - firstEventTsB2

            tEventTSB1 = ROOT.TText(x, 0.2, 'Event TS Box 1: %d'%(eventTSRelB1))
            tEventTSB1.SetTextSize(textSize)
            tEventTSB1.Draw()

            if args.interactive:
                canv.Update()
                raw_input("At event %d, Press Enter to continue..."%nEvent)

            if not args.interactive:
                wfCanvases.append(canv)


    print "\n=> %u events read" % nEvent

    inFile.read(4)
    Q, = struct.unpack('<Q', inFile.read(8))

    inFile.close()

    tree.Write()
    print '=> written to '+outFileName

    if str(hex(Q)[2:-1]) != 'babaabba00caffee':
        raise 'corrupt end of file'

    print 'valid end of file'

    if args.diagnostics:
        canvas = ROOT.TCanvas()
        ROOT.gDirectory.mkdir('raw')
        ROOT.gDirectory.cd('raw')
        for canv in wfCanvases:
            canv.Write()
            #canv.SaveAs('canvas%d.pdf'%nEvent)
    end = time.time()
    print 'time elapsed', end - start
    print 'time per event', (end-start)/float(nEvent)


if __name__ == '__main__':
    main()
