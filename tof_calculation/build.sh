#/bin/bash

python plot.py
mkdir out
pdflatex -interaction nonstopmode -halt-on-error -output-directory out tof.tex
mv out/tof.pdf .
rm -r out
