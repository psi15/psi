/*

*/
#include "TSystem.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TFile.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "TString.h"
#include <iostream>
#include <fstream>
#include <vector>
#include "TCanvas.h"
#include "TMath.h"
#include "TF1.h"
#include "TStyle.h"
#include <sys/stat.h>
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TChain.h"
#include "TLine.h"
#include "TLegend.h"
#include "TEllipse.h"

#include "TApplication.h"

using namespace ROOT;
using namespace std;


#include "../util.h"

double tac1conv(double au) {
    return -18.7825387144 + au * 0.0259258412451;
}

double tac1_to_pi(double pi) {
    return (0.01152*pi + 465.48819)*10.;
}

double pi_to_tac1(double tac1) {
    return ((tac1-465.48819)/0.01152)/10.;
}

double tac2_to_pi(double pi) {
    return (0.00304*pi + 542.47135)*10.;
}

double pi_to_tac2(double tac2) {
    return ((tac2-542.47135)/0.00304)/10.;
}

int main(int argc, char *argv[]) {
    gStyle->SetOptFit(1);
    gStyle->SetOptStat(0);
    gStyle->SetTitleOffset(1.5, "Y");

    gStyle->SetPaperSize(20,26);

    if (argc < 3) {
        cout << "not enough arguments: input file, output file" << endl;
        return 1;
    }

    char *fname = argv[1];
    char *outfname = argv[2];

    auto c = new TCanvas("c", "c", 800, 600);
    c->SaveAs("dummy.pdf");


    gPad->SetTickx();
    gPad->SetTicky();


    __("opening file at " << fname)

    auto inFile = TFile::Open(fname);

    TTreeReader reader("physics", inFile);
    int nEvents = reader.GetEntries(true);


    __("looping over " << nEvents << " events")

    // READER VALUES
    auto calSumIntegralCalibrated = new TTreeReaderValue<double>(reader, "calSumIntegralCalibrated");
    auto tac1Time = new TTreeReaderValue<double>(reader, "tac1Time");
    auto tac1Amplitude = new TTreeReaderValue<double>(reader, "tac1Amplitude");
    auto tac2Time = new TTreeReaderValue<double>(reader, "tac2Time");
    auto tac2Amplitude = new TTreeReaderValue<double>(reader, "tac2Amplitude");
    auto pistopTriggerBin = new TTreeReaderValue<double>(reader, "pistopTriggerBin");
    auto pistopNum = new TTreeReaderValue<int>(reader, "pistopNum");

    // HISTOGRAMS
    TH1D calSumHistNoCuts(   "calSumHistNoCuts",  "calSumHistNoCuts", 80, 20, 180);
    calSumHistNoCuts.GetXaxis()->SetTitle("E [MeV]");
    calSumHistNoCuts.GetYaxis()->SetTitle(Form("dN/dE [1/%.0f MeV]", calSumHistNoCuts.GetBinWidth(1)));


    TH1D tac1TimeHistRaw(       "tac1TimeRaw",          "tac1TimeRaw",          200*10,   400*10,  600*10);
    TH1D tac2TimeHistRaw(       "tac2TimeRaw",          "tac2TimeRaw",          200*10,   400*10,  600*10);

    TH1D tac1TimeHist(       "tac1Time",          "tac1Time",          200,   400*10,  600*10);
    tac1TimeHist.GetXaxis()->SetTitle("t [ns]");
    tac1TimeHist.GetYaxis()->SetTitle(Form("dN/dt [1/%.0f ns]", tac1TimeHist.GetBinWidth(1)));

    TH1D tac2TimeHist(       "tac2Time",          "tac2Time",          200,   400*10,  600*10);


    TH2D tac1CalNoCuts("tac1CalNoCuts", "tac1CalNoCuts", 170, 500*10, 570*10, 100, 0, 160);
    tac1CalNoCuts.GetYaxis()->SetTitle("calSum [MeV]");
    tac1CalNoCuts.GetXaxis()->SetTitle("tac1 [ns]");
    TH2D tac2CalNoCuts("tac2CalNoCuts", "tac2CalNoCuts", 170, 500*10, 570*10, 100, 0, 160);
    tac2CalNoCuts.GetYaxis()->SetTitle("calSum [MeV]");
    tac2CalNoCuts.GetXaxis()->SetTitle("tac1 [ns]");

    TH2D tac1CalCuts("tac1CalCuts", "tac1CalCuts", 200*10, 500*10, 600*10, 100, 0, 160);
    tac1CalCuts.GetYaxis()->SetTitle("calSum [MeV]");
    tac1CalCuts.GetXaxis()->SetTitle("tac1 [ns]");


    TH1D calSumTac1HistCut("calSumTac1HistCut", "calSumTac1HistCut", 80, 20, 180);
    calSumTac1HistCut.GetXaxis()->SetTitle("E [MeV]");
    calSumTac1HistCut.GetYaxis()->SetTitle(Form("dN/dE [1/%.0f MeV]", calSumTac1HistCut.GetBinWidth(1)));

    TH1D calSumTac2HistCut("calSumTac2HistCut", "calSumTac2HistCut", 80, 20, 180);

    TH2D tac1pistop("tac1pistop", "tac1pistop", 140, 460, 600, 140, 460*10, 600*10);
    tac1pistop.GetXaxis()->SetTitle("#pi-stop bin");
    tac1pistop.GetYaxis()->SetTitle("tac1 time [ns]");


    TH2D tac2pistop("tac2pistop", "tac2pistop", 140, 460, 600, 140, 460*10, 600*10);


    // CUT VARIABLES
    double tac1Min = 548.*10;
    double tac1Max = 550.9*10;

    double tac2Min = 551.*10;
    double tac2Max = 551.5*10;


    print(tac1Min)
    print(tac1Max)

    print(tac2Min)
    print(tac2Max)


    int n = 0;
    while(reader.Next()) {
        n++;

        tac1TimeHistRaw.Fill(**tac1Time*10);
        tac2TimeHistRaw.Fill(**tac2Time*10);

        //calSumHistNoCuts.Fill(**calSumIntegralCalibrated);
        //tac1CalScatterHistNoCuts.Fill(**calSumIntegralCalibrated, **tac1Time);

        if(**pistopNum == 1 && **pistopTriggerBin > 200) {

            //cout << "tac1_to_pi" << tac1_to_pi(**tac1Amplitude) << endl;
            //cout << "pistopTriggerBin" << **pistopTriggerBin << endl;
            //cout << fabs(tac1_to_pi(**tac1Amplitude)-**pistopTriggerBin) << endl;
            // check if tac is far away from where it should be (drift?)
            if(fabs(tac1_to_pi(**tac1Amplitude)-**pistopTriggerBin*10) < 50) {
                tac1pistop.Fill(**pistopTriggerBin, **tac1Time*10);

                tac1TimeHist.Fill(**tac1Time*10);

                // TAC1 ok
                tac1CalNoCuts.Fill(**tac1Time*10, **calSumIntegralCalibrated);
                calSumHistNoCuts.Fill(**calSumIntegralCalibrated);

                if(tac1Min < **tac1Time*10 && **tac1Time*10 < tac1Max) {
                    calSumTac1HistCut.Fill(**calSumIntegralCalibrated);
                    tac1CalCuts.Fill(**tac1Time*10, **calSumIntegralCalibrated);
                }
            }

            // same for tac2
            if(fabs(tac2_to_pi(**tac2Amplitude)-**pistopTriggerBin*10) < 50) {
                tac2pistop.Fill(**pistopTriggerBin, **tac2Time*10);


                tac2TimeHist.Fill(**tac2Time*10);

                // TAC2 ok
                tac2CalNoCuts.Fill(**tac1Time*10, **calSumIntegralCalibrated);

                if(tac2Min < **tac2Time*10 && **tac2Time*10 < tac2Max) {
                    calSumTac2HistCut.Fill(**calSumIntegralCalibrated);
                }
            }

        }



        if (n % (nEvents / 5) == 0) {
            cout << std::fixed << std::setw(7) << std::setprecision(2) << 100*n/(double)nEvents << "%" << endl;
        }

    }


    print(calSumTac1HistCut.GetEntries())

    c->SetRightMargin(0.15);
    tac1pistop.SetTitle("");
    tac1pistop.Draw("colz");
    c->SaveAs("tac1pistop.pdf");

    tac2pistop.Draw("colz");
    c->SaveAs("tac2pistop.pdf");
    c->SetRightMargin(0.1);

    c->SetLogz();

    gPad->SetTheta(60); // default is 30
    gPad->SetPhi(170); // default is 30
    gPad->Update();
    tac1CalNoCuts.SetTitle("");
    tac1CalNoCuts.Draw("colz");

    // draw cut
    TLine tac1MaxLine(tac1Max, 0, tac1Max, 160);
    tac1MaxLine.SetLineColor(kRed);
    tac1MaxLine.Draw();

    TLine tac1MinLine(tac1Min, 0, tac1Min, 160);
    tac1MinLine.SetLineColor(kRed);
    tac1MinLine.Draw();

    TEllipse promptCircle = TEllipse(5495, 79, 20, 5);
    promptCircle.SetFillStyle(0);
    promptCircle.SetLineColor(kRed);
    promptCircle.Draw();
    c->SaveAs("tac1CalNoCuts.pdf");

    c->SetRightMargin(0.15);
    tac1CalCuts.Draw("colz");
    c->SaveAs("tac1CalCuts.pdf");

    tac2CalNoCuts.Draw("colz");
    c->SaveAs("tac2CalNoCuts.pdf");
    c->SetRightMargin(0.1);

    c->SetLogz(0);

    c->SetLogy();


    tac1TimeHist.Draw();
    // draw cuts
    TLine tac1MinLine2(tac1Min, 0, tac1Min, tac1TimeHist.GetMaximum());
    tac1MinLine2.SetLineColor(kRed);
    tac1MinLine2.Draw();
    TLine tac1MaxLine2(tac1Max, 0, tac1Max, tac1TimeHist.GetMaximum());
    tac1MaxLine2.SetLineColor(kRed);
    tac1MaxLine2.Draw();
    c->SaveAs("tac1TimeHist.pdf");

    tac1TimeHistRaw.Draw();
    // draw cuts
    tac1MinLine2.Draw();
    tac1MaxLine2.Draw();
    c->SaveAs("tac1TimeHistRaw.pdf");

    c->SetLogy(1);

    calSumHistNoCuts.SetLineColor(kBlack);
    calSumHistNoCuts.GetYaxis()->SetRangeUser(1,calSumHistNoCuts.GetMaximum()*1.1);
    calSumHistNoCuts.Draw();

    calSumTac1HistCut.SetLineColor(kRed);
    calSumTac1HistCut.Draw("same");

    calSumTac2HistCut.SetLineColor(kBlue);
    calSumTac2HistCut.Draw("same");


    c->BuildLegend(0.5,0.47, 0.88, 0.68);
    c->SaveAs("calSum.pdf");

    c->Clear();

    calSumTac1HistCut.SetLineColor(kBlue);
    calSumTac1HistCut.SetTitle("");
    calSumTac1HistCut.Draw("");
    c->SaveAs("calSumTac1HistCut.pdf");

    calSumHistNoCuts.SetLineColor(kBlue);
    calSumHistNoCuts.SetTitle("");
    calSumHistNoCuts.Draw();
    c->SaveAs("calSumHistNoCuts.pdf");

    auto outFile = TFile::Open(outfname, "RECREATE");
    calSumTac1HistCut.Write();
    tac1pistop.Write();
    tac1CalNoCuts.Write();
    calSumHistNoCuts.Write();
    outFile->Close();


    // write calSumTac1HistCut to file as .csv
    std::ofstream outStream("calSumTac1HistCut.csv");
    for(int b=1;b<=calSumTac1HistCut.GetNbinsX();b++) {
        outStream << calSumTac1HistCut.GetBinCenter(b) << ";" << calSumTac1HistCut.GetBinContent(b) << endl;
    }
    outStream.close();

    cout << "total number of events in cal sum " << calSumHistNoCuts.Integral() << endl;
    std::ofstream outStream2("calSum.csv");
    for(int b=1;b<=calSumHistNoCuts.GetNbinsX();b++) {
        outStream2 << calSumTac1HistCut.GetBinCenter(b) << ";" << calSumHistNoCuts.GetBinContent(b) << endl;
    }
    outStream2.close();

    cout << "total number of events in tac1 hist " << tac1TimeHist.Integral() << endl;



    std::ofstream outStream3("time.csv");
    for(int b=1;b<=tac1TimeHist.GetNbinsX();b++) {
        outStream3 << tac1TimeHist.GetBinCenter(b) << ";" << tac1TimeHist.GetBinContent(b) << endl;
    }
    outStream3.close();

    return 0;
}
