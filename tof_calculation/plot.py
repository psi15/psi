import ROOT
from math import sqrt

canvas = ROOT.TCanvas()

npx = 400

#deltaTepi = ROOT.TF1("deltaTepi", "([4]+[0]/[1]*(sqrt(1+([2]^2)/(x^2)) - sqrt(1+([3]^2)/(x^2))))", 90, 400);

class fn:
    def __call__(self, p, par):
        d_F, c, m_pi, m_e, offset = par
        #print d_F, c, m_pi, m_e, offset
        return ((offset + d_F/c * ( sqrt(1 + (m_pi**2/p[0]**2)) - sqrt(1 + (m_e**2/p[0]**2)) )) % 20e-9 )*1e9

fs = []

for i in range(5):
    deltaTepi = ROOT.TF1("deltaTepi%u"%i, fn(), 50, 400, 5);
    deltaTepi.SetTitle('')
    deltaTepi.SetNpx(npx)

    #if i is 0:
    #    deltaTepi.GetXaxis().SetRangeUser(5, 60)

    deltaTepi.SetParameter(0, 23.4) # d_F
    deltaTepi.SetParameter(1, 3e8) # c
    deltaTepi.SetParameter(2, 130) # m_pi
    deltaTepi.SetParameter(3, 0.511) # m_e
    deltaTepi.SetParameter(4, i*20e-9)

    deltaTepi.GetXaxis().SetTitle('p [MeV]')
    deltaTepi.GetYaxis().SetTitle('#Delta#mbox{t_{F}} % 20 [ns]')
    deltaTepi.Draw('' if i is 0 else 'same')
    fs.append(deltaTepi)

for i in range(5):
    deltaTemu = ROOT.TF1("deltaTemu", fn(), 50, 400, 5);
    deltaTemu.SetNpx(npx)

    deltaTemu.SetParameter(0, 23.4) # d_F
    deltaTemu.SetParameter(1, 3e8) # c
    deltaTemu.SetParameter(2, 105) # m_mu
    deltaTemu.SetParameter(3, 0.511) # m_e
    deltaTemu.SetParameter(4, i*20e-9)

    deltaTemu.SetLineColor(ROOT.kBlue)
    deltaTemu.Draw('same')
    fs.append(deltaTemu)

leg = ROOT.TLegend(0.75, 0.75, 0.9, 0.9)

leg.AddEntry(deltaTepi, '#Delta#mbox{t_{#pi#mbox{e}}}')
leg.AddEntry(deltaTemu, '#Delta#mbox{t_{e#mbox{#mu}}}')

leg.Draw()


canvas.SaveAs('deltaT.pdf')


ROOT.gStyle.SetOptStat(0)




separation = 10


hists = []
for i in range(8):
    h = ROOT.TH1D('h%u'%i, 'h%u'%i, 100, -20, 80)
    h.GetYaxis().SetRangeUser(0, 1600)
    hists.append(h)


hists[0].SetTitle('Separation of e and #pi in ToF measurement @p=250MeV')
hists[0].GetXaxis().SetTitle('t [ns]')
hists[0].GetYaxis().SetTitle('AU')

r3 = ROOT.TRandom3()

for i in range(10000):
    for idx, h in enumerate(hists):
        h.Fill(r3.Gaus(0+idx*separation, 3))

for idx, h in enumerate(hists):
    if(idx % 2 is 0):
        h.SetLineColor(ROOT.kRed)
    h.Draw('' if idx is 0 else 'same')


for idx, h in enumerate(hists):
    t = ROOT.TLatex()
    t.SetNDC(False)
    #print 10+idx, h.GetMaximumBin(), h.GetMaximum()

    order = idx/2+1

    if(idx % 2 is 0):
        label = 'e_{%u}' % (order)
    else:
        label = '#pi_{%u}' % (order)


    t.DrawLatex(separation*idx, 1400, label)

canvas.SaveAs('p250MeVSeparation.pdf')
