import psiutil
import ROOT
import numpy as n
import scipy as sp
import operator
import math
from psiutil import kill

def amplitude_plus_minus(ch, output=False):
    if output:
        print 'AMPLITUDE WITH OUTPUT'
        c = ROOT.TCanvas()
        c.Divide(2,2)
        c.cd(1)
        rawHist = psiutil.channelArrayToHistogram(ch, 'raw')
        rawHist.Draw()
        c.cd(2)

    #amplitudeStd = ch.std()

    noiseEstimation = ch[0:100].std()


    amplitudeUncertainty = 0.5*math.sqrt(2)*noiseEstimation
    amplitude = (ch.max() - ch.min())/2.

    if output:
        print 'amplitude', amplitude
        print 'amplitudeUncertainty', amplitudeUncertainty
        print 'amplitudeStd', amplitudeStd
        #print 'lowEdge', meanUp, '+-', sigmaUp
        #print 'upEdge', meanLow, '+-', sigmaLow
        c.SaveAs('diag_ampl.pdf')

    return amplitude, amplitudeUncertainty



def get_dummy():
    ch = [200] * 2048
    rnd = ROOT.TRandom3()
    for i in range(200000):
        #ch[int(rnd.Gaus(1000, 200))] += 1

        bin = int(rnd.Uniform()*2048)
        ch[bin] += 1


    for i in range(2048):

        if 480 < i <= 500:
            ch[i] += (i-480)*5
        if 500 < i < 980:
            ch[i] += 100

        if 980 <= i <= 1020:
            ch[i] += (100 - (i-980)*5)

        if 1020 < i < 1500:
            ch[i] -= 100
        if 1500 <= i < 1520:
            ch[i] -= (100 - (i-1500)*5)

    return ch

if __name__ == '__main__':

    ch = get_dummy()

    for i in range(1):
        amplitude_plus_minus(ch, output=True)

    #hist = psiutil.channelArrayToHistogram(ch, 'hallo')
    #c = ROOT.TCanvas()
    #hist.Draw()
    #c.SaveAs('diag.pdf')
