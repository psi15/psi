/*
* First analysis of PSI15 data
Fit of Michel spectra with theoretical function convoluted with Gaussian



*/
#include "TROOT.h"
#include "TSystem.h"
#include "TH1F.h"
#include "TFile.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "TString.h"
#include <iostream>
#include <vector>
#include "TCanvas.h"
#include "TMath.h"
#include "TF1.h"
#include "TStyle.h"
#include <sys/stat.h>
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TChain.h"
#include "TFitResult.h"
#include "TFitResultPtr.h"
#include "TText.h"

#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooFFTConvPdf.h"
#include "RooPlot.h"
using namespace RooFit;


using namespace ROOT;
using namespace std;


#include "../util.h"


//Gaussian multplied with Michel function
//first parameter: convolution variable, second: maximum energy, third: normalization, fourth: sigma
Double_t michelgauss(Double_t *x, Double_t *par)

{
  Float_t xx =x[0];
  if(xx < par[1]){
    Double_t f = (3.*pow(xx/par[1], 2) - 2.*pow(xx/par[1], 3))*par[2] * exp(-0.5*pow((par[0]-xx)/par[3],2));
    //Double_t f = exp(-1.*pow((par[0]-xx)/par[3],2));
    return f;
  }
  else return 0.;


}

Double_t analytical_convolution(Double_t *xvar, Double_t *par) {
  double a = par[0];
  double mu = par[1];
  double sigma = par[2];
  double f = par[3];
  double A = par[4];
  double B = par[5];

  double min = par[6];
  double y = xvar[0];

  if(y < min) {
    return 0;
  }

  double res = (a*sqrt(2*M_PI)*sigma*(-2*B*pow(y - mu, 3) + 6*B*(-y + mu)*pow(sigma, 2) + 3*A*f*(pow(y - mu, 2) + pow(sigma, 2))))/pow(f, 3);

  res = res < 0 ? 0 : res;

  return res;
}

Double_t analytical_convolution_amp(Double_t *xvar, Double_t *par) {
  double a = par[0];
  double mu = par[1];
  double sigma = par[2];
  double f = par[3];
  double A = par[4];

  double min = par[5];
  double y = xvar[0];

  if(y < min) {
    return 0;
  }

  double res = (3*a*A*sqrt(2*M_PI)*sigma*(pow(y-mu, 2)*(f-2*y+2*mu)+(f-6*y+6*mu)*pow(sigma, 2)))/pow(f, 3);

  res = res < 0 ? 0 : res;

  return res;
}




// convolution of Gaussian with Michel function
//first: maximum energy, second: normalization, third: sigma

Double_t convolution(Double_t *t, Double_t *par){
  Double_t tt=t[0];
  TF1 * test = new TF1("test", michelgauss, 0, 50000, 4);
  test->SetParameter(0,tt);
  test->SetParameter(1,par[0]);
  test->SetParameter(2,par[1]);
  test->SetParameter(3,par[2]);

  Double_t f = test->Integral(tt - 10 * par[2], tt + 10 * par[2], 200);

  return f;

}

// ./main first_run_number last_run_number
// files have to be in same directory
int main(int argc, char *argv[]) {
  gStyle->SetOptFit(1);

  int usedRuns = 0;
  int usedEvents = 0;
  int runRejectedFitStatus = 0;
  int runRejectedEdgePosition = 0;
  int eventsRejectedEdgePosition = 0;

  std::vector<int> runsRejectedEdgePosition;

  if(argc < 5) {
    cout << "not enough arguments: dataDir, outputDir, runStart, runEnd" << endl;
    return 1;
  }

  bool DEBUG = false;
  if(argc > 5) {
    if(std::string(argv[5]) == "debug") {
      DEBUG = true;
    }
  }

  char *dataDir = argv[1];
  char *outputDir = argv[2];
  int runStart = stoi(argv[3]);
  int runEnd = stoi(argv[4]);

  cout << "looping over files in " << dataDir << " from " << runStart << " to " << runEnd << endl;
  cout << "running in " ;
  if(DEBUG) {
    cout << "DEBUG";
  }
  else {
    cout << "PROD";
  }

  cout << " mode";
  cout << endl << "==============================" << endl;
  cout << endl;

  string filename;
  double calibrated_edge = 105.6583715/2.; // half of m_mu from http://pdg.lbl.gov/2015/listings/rpp2015-list-muon.pdf
  std::vector<double> runnumbers;
  std::vector<double> michel_edges;
  std::vector<double> chi2_vec;
  std::vector<double> inverted_f;
  std::vector<double> amplitude_ratios;

  //loop over all file numbers
  TCanvas * c1 = new TCanvas("michel", "michel", 800, 800);

  for(int i = runStart; i <= runEnd; i++) {

    // open files in current directory
    filename = Form("%spsi15_run_%06d.root", dataDir, i);

    if(DEBUG) {
      cout << "--------------" << endl;
      cout << "RUN " << i << " => " << filename << endl;
      cout << "--------------" << endl;
    }

    //check if file exists
    if(!exists_test(filename)) {
      if(DEBUG) {
        cout << "file " << filename << " does not exist" << endl;
      }
      //return 1;
    }
    else {
      TFile * file = TFile::Open(filename.c_str());


      TH1F *Htac1amplitude = new TH1F("Htac1amplitude", "Htac1amplitude", 100, 6040, 6060);

      //set up vector of histograms for calorimeter integrals
      /*vector<TH1F*> histovector;
      for(int j = 0; j < 7; j++){
        histovector.push_back(new TH1F(Form("cal%dIntegral",j), Form("cal%dIntegral",j), sqrt(24000), 1000, 25000));
      }*/

      // set up histogram where we save the sum of all integrals
      TH1F* calSumHist = new TH1F("calSumHist", "calSumHist", (int)sqrt(7*25000), 0, 7*25000);
      calSumHist->GetXaxis()->SetTitle("energy [a.u.]");
      calSumHist->GetYaxis()->SetTitle("counts");
      calSumHist->GetYaxis()->SetTitleOffset(1.4);

      TText runLabel = TText(0.2, 0.8, Form("Run %u", i));
      runLabel.SetNDC();



      TChain* chain = new TChain("physics");
      chain->Add(filename.c_str());

      //set vector of  TTreeReader values for integrals for each calorimeter
      TTreeReader reader("physics", file);
      TTreeReader test(chain->GetTree());
      vector<TTreeReaderValue<double>*> integralvector;
      for(int j = 0; j < 7; j++){
        integralvector.push_back(new TTreeReaderValue<double>(reader, Form("cal%dIntegral",j+1)));
      }

      // fill histograms with events from full run for fit
      while (reader.Next()) {

        double calSum = 0;

        for(unsigned int j = 0; j < integralvector.size(); j++) {
          calSum = calSum + ** integralvector.at(j);
        }

        if(DEBUG) {
          //print(calSum);
        }

        calSumHist->Fill(calSum);

      }



      TH1F* hist;
      double max;
      double edge_approx;
      TF1 *michel = new TF1("michel", analytical_convolution_amp, 10e3, 180e3, 6);

      //c1->cd(2);
      //c1->SetLogy(1);

      max = calSumHist->GetMaximum();
      edge_approx = calSumHist->GetXaxis()->GetBinCenter(calSumHist->FindLastBinAbove(max/2.));

      michel->SetParameter(0, 0.00544346); // a
      michel->SetParameter(1, -3298.64); // mu
      michel->SetParameter(2, 4954.26); // sigma
      michel->SetParameter(3, 7*17658.); // f
      michel->SetParameter(4, 4.1906); // A
      //michel->SetParameter(5, 4.82782); // B
      //michel->FixParameter(4, 1); // A
      //michel->FixParameter(5, 1); // B
      michel->FixParameter(5, 10e3); // min

      michel->SetParName(0, "a");
      michel->SetParName(1, "mu");
      michel->SetParName(2, "sigma");
      michel->SetParName(3, "f");
      michel->SetParName(4, "A");
      //michel->SetParName(5, "B");
      michel->SetParName(5, "min");


      std::string fitOpt = "RMS";

      if(!DEBUG) {
        fitOpt = fitOpt + "q";
        //gROOT->ProcessLine( "gErrorIgnoreLevel = 4001;");
      }

      Int_t fitStatus = calSumHist->Fit(michel, fitOpt.c_str());

      if(DEBUG) {
        cout << "STATUS: " << fitStatus << endl;

        calSumHist->Draw();
        runLabel.Draw();
        c1->SaveAs(Form("debug_output/calSumHist_%06d.pdf", i));
      }

      if(fitStatus == -1) {
        if(DEBUG) {
          cout << "fit is not fine, skip" << endl;
        }
        runRejectedFitStatus++;
        continue;
      }


      //get Michel edges from fit
      double michel_edge = michel->GetParameter(3)/michel->GetParameter(4);
      double chi2red = michel->GetChisquare()/(double)michel->GetNDF();

      double amplitudeRatio = michel->GetParameter(4)/michel->GetParameter(5);

      // THIS IS UGLY!

      if(fabs(michel_edge - 120e3) > 20e3) {
        runRejectedEdgePosition++;
        eventsRejectedEdgePosition += calSumHist->GetEntries();
        runsRejectedEdgePosition.push_back(i);
        continue;
      }

      usedRuns++;
      usedEvents += calSumHist->GetEntries();

      runnumbers.push_back(i);

      michel_edges.push_back(michel_edge);
      chi2_vec.push_back(chi2red);

      inverted_f.push_back(1/michel_edge);
      amplitude_ratios.push_back(amplitudeRatio);

      delete michel;

      // fill tree with calibrated spectra
      std::string outFileName = Form("%spsi15_run_%06d.root", outputDir, i);

      if(DEBUG) {
        cout << "write to file " << outFileName << endl;
      }

      TFile outFile(outFileName.c_str(), "RECREATE");

      // open old tree and clone it
      TTree *inTree = (TTree*) file->Get("physics");
      TTree *outTree = inTree->CloneTree();

      // clone meta tree and write directly to new file
      ((TTree*) file->Get("meta"))->CloneTree()->Write();


      // set up new and old branches we need for the calibration
      // this uses two macros defined above
      NEW_BRANCH(calSumIntegralCalibrated, outTree)

      EX_BRANCH(cal1Integral, inTree)
      EX_BRANCH(cal2Integral, inTree)
      EX_BRANCH(cal3Integral, inTree)
      EX_BRANCH(cal4Integral, inTree)
      EX_BRANCH(cal5Integral, inTree)
      EX_BRANCH(cal6Integral, inTree)
      EX_BRANCH(cal7Integral, inTree)


      // other way to loop over events not using reader.
      // we can write selected branches this way and retain the other ones
      int nEntries = inTree->GetEntries();
      for(int i=0;i<nEntries;i++) {

        inTree->GetEntry(i);

        double calSumIntegral = cal1Integral + cal2Integral + cal3Integral + cal4Integral + cal5Integral + cal6Integral + cal7Integral;

        // calculate the shifted value relative to the edge and multiply with half muon mass
        calSumIntegralCalibrated = calibrated_edge / michel_edge * calSumIntegral;

        // fill JUST the new branches and not the entire tree
        bcalSumIntegralCalibrated->Fill();

      }

      // write tree to disk
      outTree->Write();
      outFile.Close();







      //c1->SaveAs("./michel_fits.pdf");

    }

    if((i-runStart) % 10 == 0) {
      cout << std::fixed << std::setw(7) << std::setprecision(2) << 100*(i-runStart)/(double)(runEnd-runStart) << "%";

      cout << " (" << i << ")" << endl;
    }
  }

  cout << "--------------" << endl;
  cout << "END OF RUN LOOP" << endl;
  cout << "--------------" << endl;



  TGraph * michelEdgesOverRunNumberGraph = new TGraph(runnumbers.size(), runnumbers.data(), michel_edges.data());
  michelEdgesOverRunNumberGraph->SetLineColor(1);
  TCanvas * canvas = new TCanvas();

  michelEdgesOverRunNumberGraph->Draw("ALP");
  //michelEdgesOverRunNumberGraph->SetTitle("Michel edge position over run number");
  michelEdgesOverRunNumberGraph->SetTitle("");
  michelEdgesOverRunNumberGraph->GetXaxis()->SetTitle("run number");
  michelEdgesOverRunNumberGraph->GetYaxis()->SetTitle("Michel edge position");
  //canvas->BuildLegend();
  canvas->SaveAs("./michel_fits_over_runs.pdf");

  TGraph * chi2OverRunNumberGraph = new TGraph(runnumbers.size(), runnumbers.data(), chi2_vec.data());
  chi2OverRunNumberGraph->SetLineColor(1);

  chi2OverRunNumberGraph->Draw("ALP");
  //chi2OverRunNumberGraph->SetTitle("chi2/ndof over run number");
  chi2OverRunNumberGraph->SetTitle("");
  chi2OverRunNumberGraph->GetXaxis()->SetTitle("run number");
  chi2OverRunNumberGraph->GetYaxis()->SetTitle("#chi^{2}/ndf");
  canvas->SaveAs("./chi2_ndof_over_runs.pdf");


  TGraph * amplitudeGraph = new TGraph(amplitude_ratios.size(), amplitude_ratios.data(), inverted_f.data());
  amplitudeGraph->SetLineColor(1);

  amplitudeGraph->Draw("ALP");
  amplitudeGraph->SetTitle("A/B vs. 1/f");
  amplitudeGraph->GetXaxis()->SetTitle("A/B");
  amplitudeGraph->GetYaxis()->SetTitle("1/f");
  canvas->SaveAs("./ABvsf-1.pdf");

  __("Rejected Runs:")
  for(auto const& r: runsRejectedEdgePosition) {
    print(r);
  }

  print(usedRuns)
  print(usedEvents)
  print(runRejectedEdgePosition)
  print(runRejectedFitStatus)
  print(eventsRejectedEdgePosition)

  return 0;
}
