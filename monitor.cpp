
void monitor(int runStart = -1, int runStop = -1 ){
    if (runStart>=runStop && runStop != -1 && runStart != -1){
        cout << "Stop must be bigger than Start" << endl;
        return;
        
    }
    cout << "Duuuuuuuuuuuuude" << endl;
    
    const int size(7);
    
    TChain * tree = new TChain("physics");
    TGraph* caloGraphs[7];
    for(int i=0;i<7;++i) {
        caloGraphs[i] = new TGraph();
        caloGraphs[i]->SetMarkerStyle(8);
        caloGraphs[i]->SetMarkerSize(1);
    }
    
    int caloGraphN = 0;
    
    TGraph *Sc6Graph = new TGraph();
    Sc6Graph->SetMarkerStyle(8);
    Sc6Graph->SetMarkerSize(1);
    
    //bool initSUMON = 1;
    TH1F* SUMON[size] ;
    TH1F* SUMONcut[size] ;
    float NminCalib = 0;
    float NmaxCalib = 100;
    int N = 600; // originally 200
    
    for (int i=0; i<size; ++i) {
        SUMON[i] = new TH1F(Form("SUMON%d",i), Form("SUMON%d",i), N, NminCalib, NmaxCalib);
        
        SUMONcut[i] = new TH1F(Form("SUMONcut%d",i), Form("SUMONcut%d",i), N, NminCalib, NmaxCalib);
    }

    
    // Take all processed files
    TSystemDirectory dir("./", "./");
    TList *files = dir.GetListOfFiles();
    if (files) {
        TSystemFile *file;
        TString fname;
        
        int j=0;
        
        TIter next(files);
        while ((file=(TSystemFile*)next())) {
            fname = file->GetName();
            
            
            
            if (!file->IsDirectory() && fname.EndsWith("root")) {
                int runNumber = atoi(fname(10, 6).Data());
                
                if(runStart > runNumber && runStart != -1) {
                    continue;
                }
                if(runNumber > runStop && runStop != -1) {
                    continue;
                }
                
                cout << "load: " << runNumber << endl;
                
                j = j+1;
                
                
                TFile* inFile = new TFile("./"+TString(fname.Data()));
                
                
                TTree *physics = (TTree*) inFile->Get("physics");
                TH1F* CALO[size];
                
                int V1_Bin[size];
                int V2_Bin[size];
                double V1[size];
                double V2[size];
                double N1[size];
                double N2[size];
                double Nmin = 5000;
                double Nmax = 50000;
                double E1 = 0; // [MeV]
                double E2 = 53; // [MeV]
                double m[size];
                double h[size];
                
                for (int i=0; i<size; ++i)
                {
                    physics->Draw(Form("cal%dIntegral>>calo%d(%d ,%f,%f )", i+1, i+1 , N, Nmin, Nmax));
                    
                    CALO[i] = (TH1F*) gDirectory->Get(Form("calo%d",i+1));
                    
                    CALO[i]->Smooth();
                    
                    CALO[i]->Draw();
                    
                    V2_Bin[i] = CALO[i]->FindLastBinAbove(CALO[i]->GetMaximum()/2.);
                    V2[i] = CALO[i]->GetXaxis()->GetBinCenter(V2_Bin[i]);
                    
                    caloGraphs[i]->SetPoint(caloGraphN, runNumber, V2[i]);
                    
                    //cout << "V2 [" << i << "] =" <<  V2[i] << endl;
                }
                
                //double cal1integral(0);
                //TBranch *cal1;
                
                //cout << "file number " << j << endl;
                
                for (int i=0; i<size; ++i)
                {
                    
                    physics->Draw(Form("cal%dIntegral>>calo%d(%d ,%f,%f )", i+1, i+1 , N, Nmin, Nmax));
                    
                    CALO[i] = (TH1F*) gDirectory->Get(Form("calo%d",i+1));
                    
                    CALO[i]->Smooth();
                    
                    CALO[i]->Draw();
                    
                    V1_Bin[i] = CALO[i]->GetMaximumBin();
                    
                    V1[i] = CALO[i]->GetXaxis()->GetBinCenter(V1_Bin[i]);
                    
                    N1[i] = CALO[i]->GetBinContent(V1_Bin[i]);
                    
                    N2[i] = 0.5*N1[i];
                    
                    V2_Bin[i] = CALO[i]->FindLastBinAbove(N2[i]);
                    
                    V2[i] = CALO[i]->GetXaxis()->GetBinCenter(V2_Bin[i]);
                    
                    //m[i] = (E1 - E2)/(V1[i] - V2[i]);
                    //h[i] = (E2*V1[i] - E1*V2[i])/(V1[i] - V2[i]);
                    m[i] = E2/V2[i];
                    h[i] = 0;
                    
                    
                    
                    /*cout << "V1_Bin [" << i << "] =" <<  V1_Bin[i] << endl;
                    cout << "V1 [" << i << "] =" <<  V1[i] << endl;
                    cout << "N1 [" << i << "] =" <<  N1[i] << endl;
                    cout << "N2 [" << i << "] =" <<  N2[i] << endl;
                    cout << "V2_Bin [" << i << "] =" <<  V2_Bin[i] << endl;
                    cout << "V2 [" << i << "] =" <<  V2[i] << endl;
                    cout << "m [" << i << "] =" <<  m[i] << endl;
                    cout << "h [" << i << "] =" <<  h[i] << endl;*/
                    
                }
                
                
                
                for (int i=0; i<size; ++i)
                {
                    //cout << "correction ch " << i+1 << " " << m[i] << " " << h[i] << endl;
                    physics->Draw(Form("cal%dIntegral*%f+%f>>hEne%d(%d ,%f,%f )",i+1 ,m[i], h[i], i+1, N, NminCalib, NmaxCalib) );
                    physics->Draw(Form("cal%dIntegral*%f+%f>>hEne_cut%d(%d ,%f,%f )",i+1 ,m[i], h[i], i+1, N, NminCalib, NmaxCalib), "tac1Amplitude>7200&&tac1Amplitude<7400" );
                    /*physics->Draw(Form("cal%dIntegral*%f+%f>>hEne_cut%d(%d ,%f,%f )",i+1 ,m[i], h[i], i+1, N, Nmin, Nmax), "tac1Amplitude>7000" , "Same" );
                    
                    SUMON_cut->Add((TH1F*) gDirectory->Get(Form("hEne_cut%d",i)));*/
                    
                    //cout << "test1 initSUMON:" << initSUMON << " " << i << endl;
                    
                    //if(SUMON[i] == 0)
                    //{
                    //    cout << "init SUMON: " << i << endl;
                    //    SUMON[i] = ((TH1F*)gDirectory->Get(Form("hEne%d",i+1)))->Clone();
                    //    SUMON[i]->SetName(Form("SUMON%d",i+1));
                        //SUMON[i]->SetDirectory(0);
                    //    cout << "test2" << endl;
                        
                    //} else
                    //{
                        //cout << "test2.9" << endl;
                        SUMON[i]->Add((TH1F*)gDirectory->Get(Form("hEne%d",i+1)));
                        SUMONcut[i]->Add((TH1F*)gDirectory->Get(Form("hEne_cut%d",i+1)));
                        //cout << "test3" << endl;
                    //}
                    
                    
                    
                }
                
                //if(initSUMON == 1) {initSUMON = 0;}
                
                
                
                //cout << "test4" << endl;
                
                

                
                
                
                physics->Draw(Form("Sc6TriggerBin>>Sc6TriggerBinRun%d", runNumber));
                TH1F* Sc6TriggerBinHist = (TH1F*)gDirectory->Get(Form("Sc6TriggerBinRun%d", runNumber));
                int flankPosition = Sc6TriggerBinHist->FindFirstBinAbove(Sc6TriggerBinHist->GetMaximum()/2.);
                //cout << flankPosition << endl;
                Sc6Graph->SetPoint(caloGraphN, runNumber, flankPosition);
                
                ++caloGraphN;
                
                
                inFile->Close();
                
                
                
                tree->Add("./"+TString(fname.Data()));
            }
        }
        
        TCanvas * cSUMON = new TCanvas("cSUMON");
        TLegend* SUMONlegend = new TLegend(0.1, 0.7, 0.3, 0.9);
        SUMONlegend->AddEntry(SUMON[0], "Calo_1");
        SUMON[0]->Draw();
        TH1F* SUMONSUM = (TH1F*) SUMON[0]->Clone();
        SUMONSUM->SetName("SUMONSUM");
        TH1F* SUMONSUMcut = (TH1F*) SUMONcut[0]->Clone();
        SUMONSUMcut->SetName("SUMONSUMcut");
        
        for (int i=1; i<size; ++i)
        {
            SUMONlegend->AddEntry(SUMON[i], Form("Calo_%d", i+1));
            SUMON[i]->SetLineColor(i+1);
            SUMON[i]->Draw("same");
            
            SUMONSUMcut->Add(SUMONcut[i]);
            SUMONSUM->Add(SUMON[i]);
        }
        
        SUMONSUM->Draw("Same");
        SUMONSUMcut->Draw("Same");
        SUMONSUMcut->SetLineColor(9);
        SUMONlegend->AddEntry(SUMONSUM, "Sum Calo");
        SUMONlegend->AddEntry(SUMONSUMcut, "Sum Calo with cut");
        SUMONlegend->Draw();
        
        //TH2F* Charly("SUMONSUM","Tac")
        
        
        
        TCanvas *graphCanvas = new TCanvas();
        graphCanvas->Divide(1,2);
        graphCanvas->cd(1);
        
        caloGraphs[0]->GetYaxis()->SetRangeUser(10000, 25000);
        caloGraphs[0]->GetYaxis()->SetTitle("Michel edge [AU]");
        caloGraphs[0]->GetXaxis()->SetTitle("runNumber");
        
        TLegend *caloLegend = new TLegend(0.7,0.7,0.9,0.9);
        caloLegend->AddEntry(caloGraphs[0], "cal1", "P");
        
        caloGraphs[0]->Draw("AP");
        for(int i=1;i<7;++i) {
            caloGraphs[i]->SetMarkerColor(i+1);
            caloLegend->AddEntry(caloGraphs[i], Form("cal%d", i+1));
            caloGraphs[i]->Draw("PSAME");
        }
        
        caloLegend->Draw();
        
        graphCanvas->cd(2);
        
        Sc6Graph->SetTitle("Sc6 trigger bin position");
        Sc6Graph->GetYaxis()->SetTitle("trigger bin position");
        Sc6Graph->GetXaxis()->SetTitle("runNumber");
        Sc6Graph->Draw("AP");
        
        
    }
    
    // Debug and developing mode, use only one file
    //tree->Add("basic_root/psi15_run_003440.root");
    
    //for (int i=0; i<100; ++i)
    
    
    
    
    return;
    
    
    
    
    
    // RETUUUUUUUUUUUURN ENNNND FUNCTION MONITOR!!!!!!!!!!!!!
    // RETUUUUUUUUUUUURN ENNNND FUNCTION MONITOR!!!!!!!!!!!!!
    // RETUUUUUUUUUUUURN ENNNND FUNCTION MONITOR!!!!!!!!!!!!!
    // RETUUUUUUUUUUUURN ENNNND FUNCTION MONITOR!!!!!!!!!!!!!
    // RETUUUUUUUUUUUURN ENNNND FUNCTION MONITOR!!!!!!!!!!!!!
    
    
    

    
    
    
    /*TCanvas* viligen=new TCanvas("viligen");
    viligen->Divide(3,2);
    
    gStyle->SetOptStat(0);
    // ------------------------------------------------------------------- //
    
    
    viligen->cd(1);
    tree->Draw("tac1Amplitude:pistopTriggerBin>>tac1pistop(200,420,620,8000,0,8000)", "", "colz");
    TH2F * tac1pistop = (TH2F*)gDirectory->Get("tac1pistop");
    tac1pistop->GetXaxis()->SetTitle("pistop [10ns]");
    tac1pistop->GetYaxis()->SetTitle("tac1 [AU]");
    
    viligen->cd(2);
    tree->Draw("pistop2TriggerBin:pistopTriggerBin", "", "colz");
    
    
    viligen->cd(3);
    tree->Draw("tac1Amplitude:tac2Amplitude");
    
    viligen->cd(4);
    tree->Draw("tac2Amplitude:pistopTriggerBin>>tac2pistop(200,420,620,8000,0,8000)", "", "colz");
    TH2F * tac2pistop = (TH2F*)gDirectory->Get("tac2pistop");
    tac2pistop->GetXaxis()->SetTitle("pistop [10ns]");
    tac2pistop->GetYaxis()->SetTitle("tac1 [AU]");
    
    viligen->cd(5);
    tree->Draw("cal1Integral:tac1Amplitude>>cal1tac1(8000,0,8000,250,0,25000)","","colz");
    TH2F * cal1tac1 = (TH2F*)gDirectory->Get("cal1tac1");
    cal1tac1->GetXaxis()->SetTitle("tac1 [AU]");
    cal1tac1->GetYaxis()->SetTitle("energy [AU]");
    
    
    viligen->cd(6);
    tree->Draw("Sc4TriggerBin>>vetopistop");
    TH1F* vetopistop = (TH1F*)gDirectory->Get("vetopistop");
    vetopistop->SetLineColor(kRed);
    vetopistop->GetXaxis()->SetTitle("triggerBin [10ns]");
    
    tree->Draw("pistopTriggerBin>>pistop","","SAME");
    TH1F* pistop = (TH1F*)gDirectory->Get("pistop");
    
    
    tree->Draw("Sc6TriggerBin>>Sc6","","SAME");
    TH1F* Sc6 = (TH1F*)gDirectory->Get("Sc6");
    Sc6->SetLineColor(kGreen);
    
    
    TLegend* legend=new TLegend(0.5,0.5,0.9,0.9);
    legend->AddEntry(vetopistop,"veto");
    legend->AddEntry(pistop,"pistop");
    legend->AddEntry(Sc6,"Sc6");
    legend->Draw();
    
    // ----------------------- Energies ------------------------- //
    */
    TCanvas* viligen2=new TCanvas("viligen2");
    viligen2->Divide(2,2);
    
    viligen2->cd(1);
    tree->Draw("tac1Amplitude>>tac1");
    tree->Draw("tac1Amplitude>>tac1energycut", "cal1Integral>21000&&cal7Integral>21000", "SAME");
    TH1F* tac1energycut = (TH1F*)gDirectory->Get("tac1energycut");
    tac1energycut->SetLineColor(kRed);
    
    viligen2->cd(2);
    tree->Draw("tac2Amplitude>>tac2(300,500,3500)");
    tree->Draw("tac2Amplitude>>tac2energycut", "cal1Integral>21000&&cal7Integral>21000", "SAME");
    TH1F* tac2energycut = (TH1F*)gDirectory->Get("tac2energycut");
    tac2energycut->SetLineColor(kRed);
    
    viligen2->cd(3);
    tree->Draw("pistopTriggerBin>>trig(220,380,600)");
    
    viligen2->cd(4);
    
    //TFile* file = new TFile("all.root", "read");
    
    //TTree* tree = ((TTree*) file->Get("physics"));
    /*
    int N = 200;
    double Nmin = 5000;
    double Nmax = 50000;
    const int size(7);
    TH1F* CALO[size];
    int V1_Bin[size];
    int V1[size];
    double N1[size];
    double N2[size];
    int V2_Bin[size];
    double V2[size];
    double E1 = 40; // [MeV]
    double E2 = 53; // [MeV]
    double m[size];
    double h[size];
    
    //double cal1integral(0);
    //TBranch *cal1;
    
    
    for (int i=0; i<size; ++i)
    {
        
        tree->Draw(Form("cal%dIntegral>>calo%d(%d ,%f,%f )", i+1, i+1 , N, Nmin, Nmax));
        
        CALO[i] = (TH1F*) gDirectory->Get(Form("calo%d",i+1));
        
        CALO[i]->Smooth();
        
        CALO[i]->Draw();
        
        V1_Bin[i] = CALO[i]->GetMaximumBin();
        
        V1[i] = CALO[i]->GetXaxis()->GetBinCenter(V1_Bin[i]);
        
        N1[i] = CALO[i]->GetBinContent(V1_Bin[i]);
        
        N2[i] = 0.5*N1[i];
        
        V2_Bin[i] = CALO[i]->FindLastBinAbove(N2[i]);
        
        V2[i] = CALO[i]->GetXaxis()->GetBinCenter(V2_Bin[i]);
        
        //m[i] = (E1 - E2)/(V1[i] - V2[i]);
        //h[i] = (E2*V1[i] - E1*V2[i])/(V1[i] - V2[i]);
        m[i] = E2/V2[i];
        h[i] = 0;
        
        
        
        cout << "V1_Bin [" << i << "] =" <<  V1_Bin[i] << endl;
        cout << "V1 [" << i << "] =" <<  V1[i] << endl;
        cout << "N1 [" << i << "] =" <<  N1[i] << endl;
        cout << "N2 [" << i << "] =" <<  N2[i] << endl;
        cout << "V2_Bin [" << i << "] =" <<  V2_Bin[i] << endl;
        cout << "V2 [" << i << "] =" <<  V2[i] << endl;
        cout << "m [" << i << "] =" <<  m[i] << endl;
        cout << "h [" << i << "] =" <<  h[i] << endl;
        
        
        
    }
    
    Nmin = 0;
    Nmax = 100;
    
    tree->Draw(Form("cal%dIntegral*%f+%f>>hEne%d(%d ,%f,%f )",1 ,m[0], h[0], 1 , N, Nmin, Nmax));
    tree->Draw(Form("cal%dIntegral*%f+%f>>hEne_cut%d(%d ,%f,%f )",1 ,m[0], h[0], 1 , N, Nmin, Nmax),"tac1Amplitude>7000");
    
    TH1F* SUMON = (TH1F*) gDirectory->Get("hEne1")->Clone();
    SUMON->SetName("hSUMON");
    //
    TH1F* SUMON_cut = (TH1F*) gDirectory->Get("hEne_cut1")->Clone();
    SUMON_cut->SetName("hSUMON_cut");
    
    
    
    for (int i=1; i<size; ++i)
    {
        tree->Draw(Form("cal%dIntegral*%f+%f>>hEne%d(%d ,%f,%f )",i+1 ,m[i], h[i], i+1, N, Nmin, Nmax), "" , "Same" );
        SUMON->Add((TH1F*) gDirectory->Get(Form("hEne%d",i+1)));
        
        tree->Draw(Form("cal%dIntegral*%f+%f>>hEne_cut%d(%d ,%f,%f )",i+1 ,m[i], h[i], i+1, N, Nmin, Nmax), "tac1Amplitude>7000" , "Same" );
        SUMON_cut->Add((TH1F*) gDirectory->Get(Form("hEne_cut%d",i+1)));
        
    }
    
    SUMON->Draw("same");
    SUMON_cut->SetLineColor(2);
    SUMON_cut->Draw("same");*/

}