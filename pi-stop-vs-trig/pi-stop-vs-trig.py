#! /usr/bin/env python
import struct
import datetime as dt
import ctypes
import ROOT
import argparse
import numpy as np
import sys
import re
import multiprocessing as mp
import time
import ConfigParser
import inspect
import os
import math

import psiutil

def main():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('infile', nargs='?', help='input file')

    args = parser.parse_args()

    c = ROOT.TCanvas()
    c.Divide(2,2)


    infile = ROOT.TFile(args.infile, 'READ')
    physics = infile.Get('physics')

    manual = ROOT.TH1D('sc6-pistop', 'sc6-pistop', 400, 0, 400)
    scatter = ROOT.TH2D('tac1_vs_scint', 'tac1_vs_scint', 800, 0, 400, 800, 0, 8000)

    for event in physics:
        manual.Fill(event.Sc6triggerBin - event.pistopTriggerBin)
        scatter.Fill(event.Sc6triggerBin - event.pistopTriggerBin, event.tac1amplitude)


    c.cd(1)
    manual.Draw()

    c.cd(2)
    scatter.Draw('colz')


    c.Update()

    raw_input('')



if __name__ == '__main__':
    main()
