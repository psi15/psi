/*
 * Example macro to show how to load trees
 * Authors: Simon Corrodi, Dorothea vom Bruch
 * August 2014
 */

#include "TH1F.h"
#include "TFile.h"
#include "TString.h"
#include <iostream>
#include <string>
#include "TTree.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TStyle.h"

using namespace std;

void lifetime( ) {

  gStyle->SetOptFit(0111);
  gStyle->SetOptStat(0);
  gStyle->SetTitleOffset(1.5, "Y");

  /*
   * Load data from file
   * TFile(const char* fname, Option_t* option = "", const char* ftitle = "", Int_t compress = 1)
   */

  TFile *file = new TFile("combined_slim.root","READ");
  
  /* 
   * Load the tree, show the # of entries and its branches
   */
  TTree *physics = (TTree*)file->Get("physics");
  TObjArray* branchList = physics->GetListOfBranches();
  cout << "Total entries: " << physics->GetEntries() << endl;
  cout << "Branches: " << endl;

  for (int i = 0; i < 2; i++) {
    cout << " -" << branchList->At(i)->GetName() << endl;  // Get name of each branch
  }

  TCanvas *c2 = new TCanvas("c2","Canvas for htime",100,100,800,600);
  c2->SaveAs("dummy.pdf");

  gPad->SetTickx();
  gPad->SetTicky();

  c2->cd(1);

  TH1F * hEnergy = (TH1F*)gDirectory->Get("hEnergy");

  physics->Draw("tac1Time>>htime(700,470,554)","calSumIntegralCalibrated<80&calSumIntegralCalibrated>0");


  TH1F * htime = (TH1F*)gDirectory->Get("htime");
  TF1 *fit = new TF1("fit","[0]*(exp(-([2]-x)/219.7)-exp(-[1]*([2]-x)))",540,551);
  fit->SetParameter(0,10000);
  fit->SetParameter(1,0.1);
  fit->SetParameter(2,552.2);
  htime->Fit("fit","","",480,551);
  htime->GetXaxis()->SetTitle("t [10ns]");
  htime->GetYaxis()->SetTitle(Form("dN/dt [1/%.3f ns]", 10*htime->GetBinWidth(1)));
  htime->GetYaxis()->SetRangeUser(htime->GetMinimum(), htime->GetMaximum()*1.8);

  htime->SetTitle("");
  //fit->Draw();
  htime->Draw();
  c2->SaveAs("lifetime_fit.pdf","pdf");


  physics->Draw("tac1Time>>htimePiE(180,470,555)", "calSumIntegralCalibrated>76&calSumIntegralCalibrated<80");
  TH1F *htimePiE = (TH1F*)gDirectory->Get("htimePiE");

  TF1 *piEFit = new TF1("piEFit", "[0]*exp(-([2]-x)/([1]/10.)) + [3]*x+[4]");
  //TF1 *piEFit = new TF1("piEFit", "[0]*exp(-([2]-x)/([1]/10.)) + [3]*exp(-([4]-x)/([5]/10.))");
  piEFit->SetParName(0, "A");
  piEFit->SetParName(1, "tau");
  piEFit->SetParName(2, "offset");
  piEFit->SetParName(3, "slope");
  piEFit->SetParName(4, "linear offset");



  piEFit->SetParameter(0, 10);
  piEFit->SetParameter(1, 26.);
  piEFit->SetParameter(2, 550);
  /*piEFit->SetParameter(3, 10);
  piEFit->SetParameter(4, 2200.);
  piEFit->SetParameter(5, 550);*/

  htimePiE->Fit("piEFit", "", "", 480, 550.5);

  c2->SetLogy(1);
  htimePiE->SetTitle("");
  htimePiE->GetXaxis()->SetTitle("t [10ns]");
  htimePiE->GetYaxis()->SetTitle(Form("dN/dt [1/%.3f ns]", 10*htimePiE->GetBinWidth(1)));
  htimePiE->GetYaxis()->SetRangeUser(1, htimePiE->GetMaximum()*2*100);
  htimePiE->Draw();
  c2->SaveAs("htimePiE.pdf");

  /*
   * Plot energy histograms
   */ 
  TFile *f = new TFile("lifetime_fit.root","RECREATE","");
  htime->Write();
  htimePiE->Write();
  f->Close();



}

int main() {
  lifetime();
  return 0;
}