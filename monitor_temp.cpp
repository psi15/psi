
void fit_muon_lifetime(int run)
{
    string test = Form("basic_root/psi15_run_%06d.root",run);
    cout << test << endl;
    TFile* inFile = new TFile(test.c_str());
    
    TTree *physics = (TTree*) inFile->Get("physics");
    physics->Draw("tac2Amplitude>>htac2(400,0,4500)");
    TH1F * histo_tac2 = (TH1F*)gDirectory->Get("tac2Amplitude");
}



void monitor_temp(int runStart = -1, int runStop = -1 ){
    gStyle->SetOptFit(111111);
    
    Double_t conv_Tac2 = 0.03; // delta_t [ns] / delta_TAC-ADCunit -- from correlation TAC1-TAC2 + TAC1-pistop
    //Double_t conv_Tac2 =	0.02754648; // old value from hardware calibration
    Double_t conv_Tac1 = 1.149e-01; // delta_t [ns] / delta_TAC-ADCunit -- from correlation TAC1-pistop
    
    
    if (runStart>=runStop && runStop != -1 && runStart != -1){
        cout << "Stop must be bigger than Start" << endl;
        return;
        
    }
    cout << "Duuuuuuuuuuuuude" << endl; // This is a code that writes "Duuuuuuuuuuuuude" in the terminal
    
    TChain * tree = new TChain("physics"); // Create a new pointer on a TChain called tree
    TGraph* caloGraphs[7]; // This is the initialisation of a 7 dimensional array of pointers on TGraphs.
    for(int i=0;i<7;++i) {
        caloGraphs[i] = new TGraph(); // We allocate some new memory.
        caloGraphs[i]->SetMarkerStyle(8); // We set some style and size of characters for the 7 Graphs
        caloGraphs[i]->SetMarkerSize(1);
    }
    int caloGraphN = 0;
    
    TGraph *Sc6Graph = new TGraph();
    Sc6Graph->SetMarkerStyle(8);
    Sc6Graph->SetMarkerSize(1);
    
    // Take all processed files
    TSystemDirectory dir("basic_root", "basic_root");
    TList *files = dir.GetListOfFiles();
    if (files) {
        TSystemFile *file;
        TString fname;
        
        
        
        
        TIter next(files); // Open the files one after another in the directory
        while ((file=(TSystemFile*)next())) {
            fname = file->GetName();
            
            // This part opens all the files that end with root in the local directory. It stops when there are no more such files.
            if (!file->IsDirectory() && fname.EndsWith("root")) {
                
                // This part is done for all the files that end with root in the local directory. It stops when there are no more such files.
                int runNumber = atoi(fname(10, 6).Data());
                
                if(runStart > runNumber && runStart != -1) {
                    continue;
                }
                if(runNumber > runStop && runStop != -1) {
                    continue;
                }
                
                cout << "load: " << runNumber << endl;
                
                TFile* inFile = new TFile("basic_root/"+TString(fname.Data()));
                
                TTree *physics = (TTree*) inFile->Get("physics");
                TH1F* CALO[7]; // Creates a 7 dimensional array of pointers on 1 Dimensional Histograms
                int N = 200; //
                int V2_Bin[7]; //
                double V2[7]; //
                double Nmin = 5000; //
                double Nmax = 50000; //
                
                for (int i=0; i<7; ++i)
                {
                    physics->Draw(Form("cal%dIntegral>>calo%d(%d ,%f,%f )", i+1, i+1 , N, Nmin, Nmax));
                    
                    CALO[i] = (TH1F*) gDirectory->Get(Form("calo%d",i+1)); // We store the data under the column calo(i+1) in the 1 dimensional histogram
                    
                    CALO[i]->Smooth(); // Smooth() makes the histogram look smoother.
                    
                    CALO[i]->Draw(); // We draw the this histogram, it's uncalibrated so far.
                    
                    V2_Bin[i] = CALO[i]->FindLastBinAbove(CALO[i]->GetMaximum()/2.); // We find the last bin (number) that is larger than the half maximum (The Michel Edge)
                    V2[i] = CALO[i]->GetXaxis()->GetBinCenter(V2_Bin[i]); // We approximate the Value to the center of the bin. The correct uncalibrated energy value should be reached in the limit where the binsize goes to zero.
                    
                    caloGraphs[i]->SetPoint(caloGraphN, runNumber, V2[i]);
                    
                    //cout << "V2 [" << i << "] =" <<  V2[i] << endl;
                }
                
                
                
                physics->Draw(Form("Sc6TriggerBin>>Sc6TriggerBinRun%d", runNumber));
                TH1F* Sc6TriggerBinHist = (TH1F*)gDirectory->Get(Form("Sc6TriggerBinRun%d", runNumber));
                int flankPosition = Sc6TriggerBinHist->FindFirstBinAbove(Sc6TriggerBinHist->GetMaximum()/2.);
                //cout << flankPosition << endl;
                Sc6Graph->SetPoint(caloGraphN, runNumber, flankPosition);
                
                ++caloGraphN;
                
                inFile->Close();
                
                
                
                tree->Add("basic_root/"+TString(fname.Data()));
            }
        }
        
        TCanvas *graphCanvas = new TCanvas();
        graphCanvas->Divide(1,2);
        graphCanvas->cd(1);
        
        caloGraphs[0]->GetYaxis()->SetRangeUser(10000, 25000);
        caloGraphs[0]->GetYaxis()->SetTitle("Michel edge [AU]");
        caloGraphs[0]->GetXaxis()->SetTitle("runNumber");
        
        TLegend *caloLegend = new TLegend(0.7,0.7,0.9,0.9);
        caloLegend->AddEntry(caloGraphs[0], "cal1", "P");
        
        caloGraphs[0]->Draw("AP");
        for(int i=1;i<7;++i) {
            caloGraphs[i]->SetMarkerColor(i+1);
            caloLegend->AddEntry(caloGraphs[i], Form("cal%d", i+1));
            caloGraphs[i]->Draw("PSAME");
        }
        
        caloLegend->Draw();
        
        graphCanvas->cd(2);
        
        Sc6Graph->SetTitle("Sc6 trigger bin position");
        Sc6Graph->GetYaxis()->SetTitle("trigger bin position");
        Sc6Graph->GetXaxis()->SetTitle("runNumber");
        Sc6Graph->Draw("AP");
        
        
    }
    
    // Debug and developing mode, use only one file
    //tree->Add("basic_root/psi15_run_003440.root");
    
    //for (int i=0; i<100; ++i)
    
    TCanvas* viligen=new TCanvas("viligen");
    viligen->Divide(3,2);
    
    gStyle->SetOptStat(0);
    // ------------------------------------------------------------------- //
    
    /////////////////////////////////////////////////////////////////////////
    
    viligen->cd(1);
    tree->Draw("tac1Amplitude:pistopTriggerBin>>tac1pistop(200,420,620,8000,0,8000)", "", "colz");
    TH2F * tac1pistop = (TH2F*)gDirectory->Get("tac1pistop");
    tac1pistop->GetXaxis()->SetTitle("pistop [10ns]");
    tac1pistop->GetYaxis()->SetTitle("tac1 [AU]");
    
    viligen->cd(2);
    tree->Draw("pistop2TriggerBin:pistopTriggerBin", "", "colz");
    
    
    viligen->cd(3);
    tree->Draw("tac1Amplitude:tac2Amplitude");
    
    viligen->cd(4);
    tree->Draw("tac2Amplitude:pistopTriggerBin>>tac2pistop(200,420,620,8000,0,8000)", "", "colz");
    TH2F * tac2pistop = (TH2F*)gDirectory->Get("tac2pistop");
    tac2pistop->GetXaxis()->SetTitle("pistop [10ns]");
    tac2pistop->GetYaxis()->SetTitle("tac1 [AU]");
    
    viligen->cd(5);
    tree->Draw("cal1Integral:tac1Amplitude>>cal1tac1(8000,0,8000,250,0,25000)","","colz");
    TH2F * cal1tac1 = (TH2F*)gDirectory->Get("cal1tac1");
    cal1tac1->GetXaxis()->SetTitle("tac1 [AU]");
    cal1tac1->GetYaxis()->SetTitle("energy [AU]");
    
    
    viligen->cd(6);
    tree->Draw("Sc4TriggerBin>>vetopistop");
    TH1F* vetopistop = (TH1F*)gDirectory->Get("vetopistop");
    vetopistop->SetLineColor(kRed);
    vetopistop->GetXaxis()->SetTitle("triggerBin [10ns]");
    
    tree->Draw("pistopTriggerBin>>pistop","","SAME");
    TH1F* pistop = (TH1F*)gDirectory->Get("pistop");
    
    
    tree->Draw("Sc6TriggerBin>>Sc6","","SAME");
    TH1F* Sc6 = (TH1F*)gDirectory->Get("Sc6");
    Sc6->SetLineColor(kGreen);
    
    
    TLegend* legend=new TLegend(0.5,0.5,0.9,0.9);
    legend->AddEntry(vetopistop,"veto");
    legend->AddEntry(pistop,"pistop");
    legend->AddEntry(Sc6,"Sc6");
    legend->Draw();
    
    // ----------------------- Energies ------------------------- //
    TCanvas* viligen2=new TCanvas("viligen2");
    viligen2->Divide(2,2);
    
    //TAC1
    viligen2->cd(1);
    tree->Draw("tac1Amplitude>>tac1");
    tree->Draw("tac1Amplitude>>tac1energycut", "cal1Integral>21000&&cal7Integral>21000", "SAME");
    tree->Draw("tac1Amplitude>>tac1cut_onepi", "pistopNum==1", "SAME");
    TH1F* h_tac1 = (TH1F*)gDirectory->Get("tac1");
    TH1F* h_tac1_energycut = (TH1F*)gDirectory->Get("tac1energycut");
    TH1F* h_tac1cut_onepi = (TH1F*)gDirectory->Get("tac1cut_onepi");
    h_tac1_energycut->SetLineColor(kRed);
    h_tac1cut_onepi->SetLineColor(kGreen);
	//Fitting pion lifetime
    Double_t tmin1 = 1000;
    Double_t tmax1 = 7400;
    TF1* pimuLifeTAC1 = new TF1("pimuLifeTAC1","[0]*(exp((x-[1])/[2]) -exp((x-[1])/[3]))",tmin1,tmax1);
    Double_t maxVal1 = h_tac1->GetBinContent(h_tac1->GetMaximumBin());
    
    cout << "maxVal: " << maxVal1 << endl;
    pimuLifeTAC1->SetParameters(maxVal1,tmax1,30000,200);
    pimuLifeTAC1->SetParLimits(0,0.5*maxVal1,2*maxVal1);
    //pimuLifeTAC1->SetParLimits(1,6500,tmax1+200);
    //pimuLifeTAC1->SetParLimits(2,10000,40000);
    //pimuLifeTAC1->SetParLimits(3,100,500);
    pimuLifeTAC1->SetParNames("Norm","xOffset","Mu Life", "Pi Life");
    h_tac1 ->Fit(pimuLifeTAC1,"","",tmin1,tmax1);
    Double_t piLifeValue_TAC1_ns = pimuLifeTAC1->GetParameter("Pi Life")*conv_Tac1; //ns
    Double_t muLifeValue_TAC1_ns = pimuLifeTAC1->GetParameter("Mu Life")*conv_Tac1; //ns
    
    //TAC2
    viligen2->cd(2);
    tree->Draw("tac2Amplitude>>tac2");
    tree->Draw("tac2Amplitude>>tac2energycut", "cal1Integral>21000&&cal7Integral>21000", "SAME");
    TH1F* tac2energycut = (TH1F*)gDirectory->Get("tac2energycut");
    tac2energycut->SetLineColor(kRed);
    tree->Draw("tac2Amplitude>>tac2cut_onepi", "pistopNum==1", "SAME");
    TH1F* tac2cut_onepi = (TH1F*)gDirectory->Get("tac2cut_onepi");
    tac2cut_onepi->SetLineColor(kGreen);
    tree->Draw("tac2Amplitude>>tac2energycut_onepi", "cal1Integral>21000&&cal7Integral>21000&&(pistopNum==1)", "SAME");
    TH1F* tac2energycut_onepi = (TH1F*)gDirectory->Get("tac2energycut_onepi");
    tac2energycut_onepi->SetLineColor(kCyan);
    
    // Fit tac2 (pi lifetime)
    TH1F* tac2 = (TH1F*)gDirectory->Get("tac2");
    Double_t tmin = 500;
    Double_t tmax = 2650;
    //   TF1* piLife = new TF1("piLife","[0]-exp((x-[2])/[1])",tmin,tmax);
    TF1* piLife = new TF1("piLife","[0]-[3]*exp((x-[2])/[1])",tmin,tmax);
    piLife->SetParameters(500,500,0,1);
    piLife->SetParNames("yOffset","Pi Life","xOffset","Norm");
    tac2->Fit(piLife,"","",tmin,tmax);
    //Converted value
    Double_t piLifeValue_TAC2_ns = piLife->GetParameter("Pi Life")*conv_Tac2; //ns
    
    
    // Draw pistopTRiggerBin
    viligen2->cd(3);
    tree->Draw("pistopTriggerBin>>trig(220,380,600)");
    tree->Draw("pistopTriggerBin>>trig_onepi(220,380,600)","pistopNum==1");//,"SAME");
    TH1F* h_pistopTriggerBin = (TH1F*)gDirectory->Get("trig");
    TH1F* h_pistopTriggerBin_onepi = (TH1F*)gDirectory->Get("trig_onepi");
    h_pistopTriggerBin_onepi->SetLineColor(kGreen);
    
    // Fit pistopTriggerBin (pi lifetime and mu lifetime)
    Double_t tmin2 = 480;
    Double_t tmax2 = 550;
    TF1* pimuLife = new TF1("pimuLife","[0]*(exp((x-[1])/[2]) -exp((x-[1])/[3]))",tmin2,tmax2);
    Double_t maxVal = h_pistopTriggerBin_onepi->GetBinContent(h_pistopTriggerBin_onepi->GetMaximumBin());
    
    cout << "maxVal: " << maxVal << endl;
    pimuLife->SetParameters(maxVal,tmax2,220,2.6);
    pimuLife->SetParLimits(0,0.5*maxVal,2*maxVal);
    pimuLife->SetParLimits(1,500,tmax2+50);
    pimuLife->SetParLimits(2,100,400);
    pimuLife->SetParLimits(3,1,5);
    pimuLife->SetParNames("Norm","xOffset","Mu Life", "Pi Life");
    h_pistopTriggerBin_onepi ->Fit(pimuLife,"","",tmin2,tmax2);
    h_pistopTriggerBin->Draw("SAME");
    h_pistopTriggerBin_onepi->GetYaxis()->SetRangeUser(0,1.5*maxVal);
    Double_t piLifeValue_pistop_ns = pimuLife->GetParameter("Pi Life")*10; //ns
    Double_t muLifeValue_pistop_ns = pimuLife->GetParameter("Mu Life")*10; //ns
    
    
    viligen2->cd(4);
    
    ////////////////////////////////////////////////////////////////////////////
    
    
    //TFile* file = new TFile("all.root", "read");
    
    //TTree* tree = ((TTree*) file->Get("physics"));
    
    //Here we take care of the CALIBRATION OF THE CALORIMETERS. We Calibrate each calorimeter of each single run to avoid long time scale modification of the response of the Calorimeters. There are three key points for calibration. E0 = 0, E1 = 40 MeV (position of the Maximum of the Michel Spectrum ) and E2 = 53 MeV (position of the falling edge of the Michel Spectrum, which we approximate by the position of the half maximum of the Michel Spectrum). The corresponding non calibrated values are called V0 = 0, V1 and V2, which we get for each calorimeter and for each run.
    
    // We search m and h such that E = m*V + h. We use in the end V0 and V2. We say E0 = m * V0 + h, E2 = m * V2 + h. Inverting the system, we get : m = (E0 - E2)/(V0 - V2) and h = (E2*V0 - E0*V2)/(V0-V2). These m and h, we compute for each calo and for each run. And then we rescale the DRAW (only) these calibrated value as histogram. and we add the contribution of all the files and then we sum up all 7 calorimeters. We observe some small nice peak around E = 70 MeV if we switch to log scale.
    
    
    int N = 200;
    double Nmin = 5000; // We'll draw the distribution of the Calorimeters energy between the uncalibrated value Nmin and Nmax
    double Nmax = 50000;
    const int size(7); // The number of Calorimeters is 7.
    TH1F* CALO[size]; // Declare a pointer on an array of 1Dimension Histograms corresponding to the 7 Calo
    int V1_Bin[size]; // The bin number corresponding  the Energy
    int V1[size];
    double N1[size]; // An array of double (Although it could be integers) corresponding to the hight (Number of entries) of the two energy reference points. N1 is the value of the maximum of the Michel Spectrum
    double N2[size]; // N2 is the value of the bin corresponding to the half maximum on the falling edge of the Michel Spectrum.
    int V2_Bin[size];
    double V2[size];
    double E1 = 40; // [MeV] The Calibrated Energy Key point 1 (position of the Maximum)
    double E2 = 53; // [MeV] The Calibrated Energy Key point 2 (poisiton of half of the Maximum (on the falling edge))
    double m[size]; // The slope of the transformation V -> E.
    double h[size]; // The hight of the transformation V -> E
    
    //double cal1integral(0);
    //TBranch *cal1;
    

    for (int i=0; i<size; ++i)
    {
        
        tree->Draw(Form("cal%dIntegral>>calo%d(%d ,%f,%f )", i+1, i+1 , N, Nmin, Nmax));
        
        CALO[i] = (TH1F*) gDirectory->Get(Form("calo%d",i+1)); // We put the values of the calorimeter(i) (i=0,6<7=size) into the 1 dimensional histogram CALO(i)
        
        CALO[i]->Smooth(); // Smooth() makes the histogram smoother with some averaging technics.
        
        CALO[i]->Draw(); // We draw the CALO[i] which is uncalibrated so far
        
        
        V1_Bin[i] = CALO[i]->GetMaximumBin(); // We get the bin number with the maximum of the Michel spetcrum for each calorimeter.
        
        
        
        V1[i] = CALO[i]->GetXaxis()->GetBinCenter(V1_Bin[i]); // We approximate the non calibrated  "energy" value as the center of the Bin corresponding to the Michel Edge.
        
        N1[i] = CALO[i]->GetBinContent(V1_Bin[i]); // N1[i] is the number of entries in the bin corresponding to the maximum of the Michel Spectrum for Calorimeter i.
        
        N2[i] = 0.5*N1[i]; // Our second point of reference for Calibration is the half maximum of the michel Spectrum. N1 is the maximum.
        
        V2_Bin[i] = CALO[i]->FindLastBinAbove(N2[i]); // We find the bin corresponding to the key point 2 (half of the maximum ). FindLastBinAbove(N2[i]) finds the LAST bin (on the right of the maximum) that is larger than N2[i] corresponding to the half maximum for calorimeter i.
        
        V2[i] = CALO[i]->GetXaxis()->GetBinCenter(V2_Bin[i]); // We approximate the position V2 of the half maximum for calorimeter i to the center of the bin we have found just above.
        
        // Using the key points 1 and 2 (did not work so well.)(E1 = 40 MeV and E2 = 53 MeV) because x value of the maximum was not so clear.
        
        //m[i] = (E1 - E2)/(V1[i] - V2[i]);
        //h[i] = (E2*V1[i] - E1*V2[i])/(V1[i] - V2[i]);
        
        // Using the key points 0 and 2. (E0 = 0 and E = 53 MeV)
        
        m[i] = E2/V2[i]; // E0 = 0, V0 = 0
        h[i] = 0; // E0 = 0, V0 = 0
        
        // Here we have found the slop and hight of the transformation V->E for calorimeter(i) (We are still in a special file.), the rescaling is now easy.
        
        // Here we juste write down the values
        cout << "V1_Bin [" << i << "] =" <<  V1_Bin[i] << endl;
        cout << "V1 [" << i << "] =" <<  V1[i] << endl;
        cout << "N1 [" << i << "] =" <<  N1[i] << endl;
        cout << "N2 [" << i << "] =" <<  N2[i] << endl;
        cout << "V2_Bin [" << i << "] =" <<  V2_Bin[i] << endl;
        cout << "V2 [" << i << "] =" <<  V2[i] << endl;
        cout << "m [" << i << "] =" <<  m[i] << endl;
        cout << "h [" << i << "] =" <<  h[i] << endl;
        
        
        
    }
    // We plot the energy between 0 and 100 MeV
    Nmin = 0; // MeV
    Nmax = 100; // MeV
    tree->Draw(Form("cal%dIntegral*%f+%f>>hEne%d(%d ,%f,%f )",1 ,m[0], h[0], 1 , N, Nmin, Nmax)); // We draw the rescaled histogram of cal(0) Integral
    tree->Draw(Form("cal%dIntegral*%f+%f>>hEne_cut%d(%d ,%f,%f )",1 ,m[0], h[0], 1 , N, Nmin, Nmax),"tac1Amplitude>7200&&tac1Amplitude<7400"); // We add a cut here on the uncalibrated time of Tac1. We cut everything that is above 7400 and everything that is below 7200.
    
    TH1F* SUMON = (TH1F*) gDirectory->Get("hEne1")->Clone(); // We clone the 1 dimensional histogram called SUMON
    SUMON->SetName("hSUMON");   // We replace the name by the evile hSUMON
    TH1F* SUMON_cut = (TH1F*) gDirectory->Get("hEne_cut1")->Clone();
    SUMON_cut->SetName("hSUMON_cut");
    
    
    // In this part we draw
    for (int i=1; i<size; ++i)
    {
        tree->Draw(Form("cal%dIntegral*%f+%f>>hEne%d(%d ,%f,%f )",i+1 ,m[i], h[i], i+1, N, Nmin, Nmax), "" , "Same" ); // This powerful method of drawing, draws the rescaled Energy according to m[i] and h[i].
        SUMON->Add((TH1F*) gDirectory->Get(Form("hEne%d",i+1))); // We add the hEne(i) histogram to SUMON, so that we get all the curves on one histogram
        
        tree->Draw(Form("cal%dIntegral*%f+%f>>hEne_cut%d(%d ,%f,%f )",i+1 ,m[i], h[i], i+1, N, Nmin, Nmax), "tac1Amplitude>7200&&tac1Amplitude<7400" , "Same" ); // Once we drew it without cuts, now we draw it with some cuts on the uncalibrated tac1 (reject t<7200 and t>7400)
        SUMON_cut->Add((TH1F*) gDirectory->Get(Form("hEne_cut%d",i+1))); // We add this last
        
    }
    // We draw the final histograms, but It is clear
    SUMON->Draw("same");
    SUMON_cut->SetLineColor(2);
    SUMON_cut->Draw("same");
    
    
    /*
     int N_Entries = tree->GetEntries();
     
     for (int i=0; i<N_Entries; ++i)
     {
     tree->GetEntry(i);
     
     
     
     }
     */
    
    //Print-outs
    cout << "Muon lifetime from TAC1:   " << muLifeValue_TAC1_ns << " ns"<<endl;
    cout << "Muon lifetime from pistop: " << muLifeValue_pistop_ns << " ns"<<endl;
    cout << "Pion lifetime from TAC1:   " << piLifeValue_TAC1_ns << " ns"<<endl;
    cout << "Pion lifetime from TAC2:   " << piLifeValue_TAC2_ns << " ns"<<endl;
    cout << "Pion lifetime from pistop: " << piLifeValue_pistop_ns << " ns"<< endl; 
    
}



/*
 file
 .! ls
 TBrowser c1
 file->Get("physics")
 (TTree*) file->Get("physics")
 TTree* tree = ((TTree*) file->Get("physics"))
 tree->Scan()
 tree->Scan()
 tree->Scan(calSumIntegral)
 tree->Scan("calSumIntegral")
 q
 tree->Draw("calSumIntegral")
 tree->Draw("calSumIntegral")
 tree->Draw("calSumIntegral")
 TFile* file_2 = new TFile("psi15_run_002")
 TFile* file_2 = new TFile("psi15_run_002630")
 TFile* file_2 = new TFile("psi15_run_002630.root"")
 TFile* file_2 = new TFile("psi15_run_002630.root")
 TFile* file_2 = new TFile("psi15_run_002630.root")
 TFile* file = new TFile("psi15_run_002630.root","read")
 
 */


