//
// Created by Paul Gessinger on 29.12.15.
//

#ifndef PSI_UTIL_H
#define PSI_UTIL_H

#include <string>
#include <sys/types.h>
#include <sys/stat.h>


#define EX_BRANCH(name, tree) double name; \
                              tree->SetBranchAddress(#name,&name);

#define NEW_BRANCH(name, tree) double name; \
                               TBranch * b ## name = tree->Branch(#name,&name, Form("%s/D", #name));


#define _(x) cout << #x;
#define __(x) cout << x << endl ;
#define print(v) _(v); cout << "=" << v << endl;

#define dout(v) if(DEBUG) { __(v) }

//check if file exists
bool exists_test (const std::string& name) {
    struct stat buffer;
    return (stat (name.c_str(), &buffer) == 0);
}

#endif //PSI_UTIL_H
